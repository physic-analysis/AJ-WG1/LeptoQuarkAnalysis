
void Draw()
{
  std::string path = "./FF.data.root";

  TFile* file = new TFile( (path).c_str(), "read");
  TH1D* Wjets_Np1_OS = (TH1D*)file->Get("HighMtCR/All/Preselection/Np1/Nb2/TauPt");
  TH1D* Wjets_Np1_SS = (TH1D*)file->Get("HighMtCR_SS/All/Preselection/Np1/Nb2/TauPt");
  TH1D* Wjets_Np3_OS = (TH1D*)file->Get("HighMtCR/All/Preselection/Np3/Nb2/TauPt");
  TH1D* Wjets_Np3_SS = (TH1D*)file->Get("HighMtCR_SS/All/Preselection/Np3/Nb2/TauPt");
  
  TH1D* ttbar_Np1_OS = (TH1D*)file->Get("HighMtCR/All/Preselection/Np1/Nb0/TauPt");
  TH1D* ttbar_Np1_SS = (TH1D*)file->Get("HighMtCR_SS/All/Preselection/Np1/Nb0/TauPt");
  TH1D* ttbar_Np3_OS = (TH1D*)file->Get("HighMtCR/All/Preselection/Np3/Nb0/TauPt");
  TH1D* ttbar_Np3_SS = (TH1D*)file->Get("HighMtCR_SS/All/Preselection/Np3/Nb0/TauPt");
  
  TH1D* QCD_Np1_Nb0_OS = (TH1D*)file->Get("CR/All/Preselection/Np1/Nb0/TauPt");
  TH1D* QCD_Np1_Nb0_SS = (TH1D*)file->Get("CR_SS/All/Preselection/Np1/Nb0/TauPt");
  TH1D* QCD_Np3_Nb0_OS = (TH1D*)file->Get("CR/All/Preselection/Np3/Nb0/TauPt");
  TH1D* QCD_Np3_Nb0_SS = (TH1D*)file->Get("CR_SS/All/Preselection/Np3/Nb0/TauPt");
  
  TH1D* QCD_Np1_Nb1_OS = (TH1D*)file->Get("CR/All/Preselection/Np1/Nb1/TauPt");
  TH1D* QCD_Np1_Nb1_SS = (TH1D*)file->Get("CR_SS/All/Preselection/Np1/Nb1/TauPt");
  TH1D* QCD_Np3_Nb1_OS = (TH1D*)file->Get("CR/All/Preselection/Np3/Nb1/TauPt");
  TH1D* QCD_Np3_Nb1_SS = (TH1D*)file->Get("CR_SS/All/Preselection/Np3/Nb1/TauPt");

  std::string all_lep        = "CR/All/Preselection/";
  std::string all_lep_HighMt = "HighMtCR/All/Preselection/";
  std::string elec           = "CR/All/Preselection/Elec/";
  std::string muon           = "CR/All/Preselection/Muon/";
  std::string elec_HighMt    = "HighMtCR/All/Preselection/Elec/";
  std::string muon_HighMt    = "HighMtCR/All/Preselection/Muon/";
  std::string rQCD_elec      = "rQCD_CR/All/Preselection/Elec/";
  std::string rQCD_muon      = "rQCD_CR/All/Preselection/Muon/";
  TH1D* FF_AllLep[2][3];
  TH1D* FF_AllLep_HighMt[2][3];
  TH1D* FF_Elec[2][3];
  TH1D* FF_Muon[2][3];
  TH1D* FF_Elec_HighMt[2][3];
  TH1D* FF_Muon_HighMt[2][3];
  TH1D* rQCD_Elec[2][3];
  TH1D* rQCD_Muon[2][3];
  for ( int nprong =0; nprong<2; nprong++){
    for ( int nbjet =0; nbjet<3; nbjet++){
      std::stringstream ss_alllep, ss_alllep_highmt, ss_elec, ss_elec_highmt, ss_muon, ss_muon_highmt, ss_rQCD_elec, ss_rQCD_muon;
      ss_alllep        << all_lep        << "Np" << 2*nprong+1 << "/Nb" << nbjet << "/TauPt";
      ss_alllep_highmt << all_lep_HighMt << "Np" << 2*nprong+1 << "/Nb" << nbjet << "/TauPt";
      ss_elec          << elec           << "Np" << 2*nprong+1 << "/Nb" << nbjet << "/TauPt";
      ss_muon          << muon           << "Np" << 2*nprong+1 << "/Nb" << nbjet << "/TauPt";
      ss_elec_highmt   << elec           << "Np" << 2*nprong+1 << "/Nb" << nbjet << "/TauPt";
      ss_muon_highmt   << muon           << "Np" << 2*nprong+1 << "/Nb" << nbjet << "/TauPt";
      ss_rQCD_elec     << rQCD_elec      << "Np" << 2*nprong+1 << "/Nb" << nbjet << "/TauPt";
      ss_rQCD_muon     << rQCD_muon      << "Np" << 2*nprong+1 << "/Nb" << nbjet << "/TauPt";
      FF_AllLep[nprong][nbjet] = (TH1D*)file->Get( ss_alllep.str().c_str() );
      FF_AllLep_HighMt[nprong][nbjet] = (TH1D*)file->Get( ss_alllep_highmt.str().c_str() );
      FF_Elec[nprong][nbjet] = (TH1D*)file->Get( ss_elec.str().c_str() );
      FF_Muon[nprong][nbjet] = (TH1D*)file->Get( ss_muon.str().c_str() );
      FF_Elec_HighMt[nprong][nbjet] = (TH1D*)file->Get( ss_elec_highmt.str().c_str() );
      FF_Muon_HighMt[nprong][nbjet] = (TH1D*)file->Get( ss_muon_highmt.str().c_str() );
      rQCD_Elec[nprong][nbjet] = (TH1D*)file->Get( ss_rQCD_elec.str().c_str() );
      rQCD_Muon[nprong][nbjet] = (TH1D*)file->Get( ss_rQCD_muon.str().c_str() );
    }
  }
  
  // OS 1-prong
  TCanvas* OS = new TCanvas("OS_Np1","OS_Np1");
  Wjets_Np1_OS  ->SetLineColor(kRed);
  ttbar_Np1_OS  ->SetLineColor(kBlack);
  QCD_Np1_Nb0_OS->SetLineColor(kGreen);
  QCD_Np1_Nb1_OS->SetLineColor(kBlue);
  
  Wjets_Np1_OS  ->SetMarkerColor(kRed);
  ttbar_Np1_OS  ->SetMarkerColor(kBlack);
  QCD_Np1_Nb0_OS->SetMarkerColor(kGreen);
  QCD_Np1_Nb1_OS->SetMarkerColor(kBlue);
  
  Wjets_Np1_OS->GetYaxis()->SetRangeUser(0,1.);
  Wjets_Np1_OS->Draw("same");
  ttbar_Np1_OS->Draw("same");
  QCD_Np1_Nb0_OS->Draw("same");
  QCD_Np1_Nb1_OS->Draw("same");
  
  // SS
  TCanvas* SS_Np1 = new TCanvas("SS_Np1","SS_Np1");
  Wjets_Np1_SS  ->SetLineColor(kRed);
  ttbar_Np1_SS  ->SetLineColor(kBlack);
  QCD_Np1_Nb0_SS->SetLineColor(kGreen);
  QCD_Np1_Nb1_SS->SetLineColor(kBlue);
  Wjets_Np1_SS  ->SetMarkerColor(kRed);
  ttbar_Np1_SS  ->SetMarkerColor(kBlack);
  QCD_Np1_Nb0_SS->SetMarkerColor(kGreen);
  QCD_Np1_Nb1_SS->SetMarkerColor(kBlue);
  Wjets_Np1_SS->GetYaxis()->SetRangeUser(0,1.);
  Wjets_Np1_SS->Draw("same");
  ttbar_Np1_SS->Draw("same");
  QCD_Np1_Nb0_SS->Draw("same");
  QCD_Np1_Nb1_SS->Draw("same");

  // 
  TCanvas* FakeFactorElec_CR[2];
  for ( int iCanvas=0; iCanvas<2; iCanvas++ ){
    FakeFactorElec_CR[iCanvas] = new TCanvas( Form("FakeFactorElec_CR%d",iCanvas), Form("FakeFactorElec_CR%d",iCanvas));
    FF_Elec[iCanvas][0]->SetMarkerColor(kRed);
    FF_Elec[iCanvas][1]->SetMarkerColor(kBlue);
    FF_Elec[iCanvas][2]->SetMarkerColor(kGreen);
    FF_Elec[iCanvas][0]->SetLineColor(kRed);
    FF_Elec[iCanvas][1]->SetLineColor(kBlue);
    FF_Elec[iCanvas][2]->SetLineColor(kGreen);
    FF_Elec[iCanvas][0]->GetYaxis()->SetRangeUser(0,1);
    FF_Elec[iCanvas][0]->Draw();
    FF_Elec[iCanvas][1]->Draw("same");
    FF_Elec[iCanvas][2]->Draw("same");
    
    FakeFactorElec_CR[iCanvas]->SaveAs(Form("FF_Elec_Np%d_CR.png",2*iCanvas+1));
  }
  
  // 
  TCanvas* FakeFactorElec_HighMtCR[2];
  for ( int iCanvas=0; iCanvas<2; iCanvas++ ){
    FakeFactorElec_HighMtCR[iCanvas] = new TCanvas( Form("FakeFactorElec_HighMtCR%d",iCanvas), Form("FakeFactorElec_HighMtCR%d",iCanvas));
    FF_Elec_HighMt[iCanvas][0]->SetMarkerColor(kRed);
    FF_Elec_HighMt[iCanvas][1]->SetMarkerColor(kBlue);
    FF_Elec_HighMt[iCanvas][2]->SetMarkerColor(kGreen);
    FF_Elec_HighMt[iCanvas][0]->SetLineColor(kRed);
    FF_Elec_HighMt[iCanvas][1]->SetLineColor(kBlue);
    FF_Elec_HighMt[iCanvas][2]->SetLineColor(kGreen);
    FF_Elec_HighMt[iCanvas][0]->GetYaxis()->SetRangeUser(0,1);
    FF_Elec_HighMt[iCanvas][0]->Draw();
    FF_Elec_HighMt[iCanvas][1]->Draw("same");
    FF_Elec_HighMt[iCanvas][2]->Draw("same");
    
    FakeFactorElec_HighMtCR[iCanvas]->SaveAs(Form("FF_Elec_Np%d_HighMtCR.png",2*iCanvas+1));
  }
  
  // 
  TCanvas* FakeFactorMuon_CR[2];
  for ( int iCanvas=0; iCanvas<2; iCanvas++ ){
    FakeFactorMuon_CR[iCanvas] = new TCanvas( Form("FakeFactorMuon_CR%d",iCanvas), Form("FakeFactorMuon_CR%d",iCanvas));
    FF_Muon[iCanvas][0]->SetMarkerColor(kRed);
    FF_Muon[iCanvas][1]->SetMarkerColor(kBlue);
    FF_Muon[iCanvas][2]->SetMarkerColor(kGreen);
    FF_Muon[iCanvas][0]->SetLineColor(kRed);
    FF_Muon[iCanvas][1]->SetLineColor(kBlue);
    FF_Muon[iCanvas][2]->SetLineColor(kGreen);
    FF_Muon[iCanvas][0]->GetYaxis()->SetRangeUser(0,1);
    FF_Muon[iCanvas][0]->Draw();
    FF_Muon[iCanvas][1]->Draw("same");
    FF_Muon[iCanvas][2]->Draw("same");
    
    FakeFactorMuon_CR[iCanvas]->SaveAs(Form("FF_Muon_Np%d_CR.png",2*iCanvas+1));
  }
  
  // 
  TCanvas* FakeFactorMuon_HighMtCR[2];
  for ( int iCanvas=0; iCanvas<2; iCanvas++ ){
    FakeFactorMuon_HighMtCR[iCanvas] = new TCanvas( Form("FakeFactorMuon_HighMtCR%d",iCanvas), Form("FakeFactorMuon_HighMtCR%d",iCanvas));
    FF_Muon_HighMt[iCanvas][0]->SetMarkerColor(kRed);
    FF_Muon_HighMt[iCanvas][1]->SetMarkerColor(kBlue);
    FF_Muon_HighMt[iCanvas][2]->SetMarkerColor(kGreen);
    FF_Muon_HighMt[iCanvas][0]->SetLineColor(kRed);
    FF_Muon_HighMt[iCanvas][1]->SetLineColor(kBlue);
    FF_Muon_HighMt[iCanvas][2]->SetLineColor(kGreen);
    FF_Muon_HighMt[iCanvas][0]->GetYaxis()->SetRangeUser(0,1);
    FF_Muon_HighMt[iCanvas][0]->Draw();
    FF_Muon_HighMt[iCanvas][1]->Draw("same");
    FF_Muon_HighMt[iCanvas][2]->Draw("same");
    FakeFactorMuon_HighMtCR[iCanvas]->SaveAs(Form("FF_Muon_Np%d_HighMtCR.png",2*iCanvas+1));
  }
  
  // 
  TCanvas* FakeFactor[2];
  for ( int iCanvas=0; iCanvas<2; iCanvas++ ){
    FakeFactor[iCanvas] = new TCanvas( Form("FakeFactor%d",iCanvas), Form("FakeFactor%d",iCanvas));
    FF_AllLep[iCanvas][0]       ->SetLineColor(kGreen);
    FF_AllLep[iCanvas][1]       ->SetLineColor(kBlue);
    FF_AllLep_HighMt[iCanvas][0]->SetLineColor(kRed);
    FF_AllLep_HighMt[iCanvas][2]->SetLineColor(kBlack);
    FF_AllLep[iCanvas][0]       ->SetMarkerColor(kGreen);
    FF_AllLep[iCanvas][1]       ->SetMarkerColor(kBlue);
    FF_AllLep_HighMt[iCanvas][0]->SetMarkerColor(kRed);
    FF_AllLep_HighMt[iCanvas][2]->SetMarkerColor(kBlack);
    
    if ( iCanvas==0 ) FF_AllLep[iCanvas][0]->GetYaxis()->SetRangeUser(0,0.5);
    if ( iCanvas==1 ) FF_AllLep[iCanvas][0]->GetYaxis()->SetRangeUser(0,0.2);
    FF_AllLep[iCanvas][0]->Draw();
    FF_AllLep[iCanvas][1]->Draw("same");
    FF_AllLep_HighMt[iCanvas][0]->Draw("same");
    FF_AllLep_HighMt[iCanvas][2]->Draw("same");
    
    FakeFactor[iCanvas]->SaveAs(Form("FF_Np%d.png",2*iCanvas+1));
  }

  TCanvas* rQCD_e[2];
  for ( int iCanvas=0; iCanvas<2; iCanvas++ ){
    rQCD_e[iCanvas] = new TCanvas(Form("rQCD_e%d",iCanvas),Form("rQCD_e%d",iCanvas));
    rQCD_Elec[iCanvas][0] ->SetLineColor(kGreen);
    rQCD_Elec[iCanvas][1] ->SetLineColor(kBlack);
    rQCD_Elec[iCanvas][2] ->SetLineColor(kRed);
    rQCD_Elec[iCanvas][0] ->SetMarkerColor(kGreen);
    rQCD_Elec[iCanvas][1] ->SetMarkerColor(kBlack);
    rQCD_Elec[iCanvas][2] ->SetMarkerColor(kRed);
    if ( iCanvas ==0 )    rQCD_Elec[iCanvas][0] ->GetYaxis()->SetRangeUser(0,0.45);
    if ( iCanvas ==1 )    rQCD_Elec[iCanvas][0] ->GetYaxis()->SetRangeUser(0,0.25);
    rQCD_Elec[iCanvas][0] ->Draw();
    rQCD_Elec[iCanvas][1] ->Draw("same");
    rQCD_Elec[iCanvas][2] ->Draw("same");

    rQCD_e[iCanvas]->SaveAs(Form("rQCD_elec_Np%d.png", 2*iCanvas+1));
  }
  
  TCanvas* rQCD_m[2];
  for ( int iCanvas=0; iCanvas<2; iCanvas++ ){
    rQCD_m[iCanvas] = new TCanvas(Form("rQCD_m%d",iCanvas),Form("rQCD_m%d",iCanvas));
    rQCD_Muon[iCanvas][0] ->SetLineColor(kGreen);
    rQCD_Muon[iCanvas][1] ->SetLineColor(kBlack);
    rQCD_Muon[iCanvas][2] ->SetLineColor(kRed);
    rQCD_Muon[iCanvas][0] ->SetMarkerColor(kGreen);
    rQCD_Muon[iCanvas][1] ->SetMarkerColor(kBlack);
    rQCD_Muon[iCanvas][2] ->SetMarkerColor(kRed);
    if ( iCanvas ==0 )    rQCD_Muon[iCanvas][0] ->GetYaxis()->SetRangeUser(0,0.45);
    if ( iCanvas ==1 )    rQCD_Muon[iCanvas][0] ->GetYaxis()->SetRangeUser(0,0.25);
    rQCD_Muon[iCanvas][0] ->Draw();
    rQCD_Muon[iCanvas][1] ->Draw("same");
    rQCD_Muon[iCanvas][2] ->Draw("same");
    
    rQCD_m[iCanvas]->SaveAs(Form("rQCD_muon_Np%d.png", 2*iCanvas+1));
  }
  
}
