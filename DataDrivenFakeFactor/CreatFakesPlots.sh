#!/bin/sh
# 
# n : events (default -1)
# --release (default 31)
# s : nsysts
# p : sample number
# q : splitJobsSyst
# t : bTaggingTruthTag
# y : sample year 
# splitTauFakes 
# ff : to calculate the fake factor, create FF_All TDirectroy.
#
# SR : tau ID 
# CR : anti-tau ID (anti-tau passes loose criteria & !medium)

SAMPLE=("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/BlueTuesday/SR_NoSys"\
        "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/BlueTuesday/CR_NoSys"\
        "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/BlueTuesday/InvIso_SR"\
        "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/BlueTuesday/InvIso_CR")

outputName=("FinalPlots.isolation_SR"\
            "FinalPlots.isolation_CR"\
            "FinalPlots.invIsol_SR"\
            "FinalPlots.invIsol_CR")
fake=(0 1 0 1)
antiTau=(0 1 0 1)
invIsol=(0 0 1 1)

for sample_number in 1 2 3 5 20 21 22 23 24 25 30 31 32 33 64 65 120 121 122 130 161 162
do
  for region in `seq 0 3`
  do
    FinalHHbbtautauLH \
      --release 31\
      -n -1 \
      -s 0 \
      -d ${SAMPLE[$region]}\
      -p ${sample_number} \
      -q 0 \
      -y 2017 \
      --analysisType TauLH \
      --outputName ${outputName[$region]}.fake${fake[$region]}_invIsol${invIsol[$region]}.root \
      --pileupReweightingFromNtuple 1  \
      --splitTauFakes 1 \
      --fake    ${fake[$region]} \
      --antiTau ${antiTau[$region]}\
      --invIsol ${invIsol[$region]} \
      --ff 1 \
      --lq3 0\
      --lq 1
  done
done
