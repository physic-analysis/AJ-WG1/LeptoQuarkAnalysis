#!/bin/sh
# 
# n events (default -1)
# --release (default 31)
# s nsysts
# p sample number
# q splitJobsSyst
# t bTaggingTruthTag
# y sample year 
# splitTauFakes 
# ff 
#
SAMPLE_MC16a=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/AtTheDriveIn/
SAMPLE_MC16d=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/BlueTuesday/SR_NoSys
SAMPLE_MC16d_AntiTau=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/BlueTuesday/CR_NoSys

SAMPLE=${SAMPLE_MC16d_AntiTau}

for sample_number in 1 2 3 5 20 21 22 23 24 25 30 31 32 33 64 65 120 121 122 130 161 162
do
FinalHHbbtautauLH \
  --release 31\
  -n -1\
  -s 0 \
  -d ${SAMPLE}\
  -p ${sample_number} \
  -q 0 \
  -y 2017 \
  --analysisType TauLH \
  --outputName FinalPlots.root \
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1\
  --fake 1 \
  --fullyDataFakes 1\
  --antiTau 0 \
  --ff 0 \
  --lq3 0\
  --lq 1\
  --outputNtupName testNtup.root
done
