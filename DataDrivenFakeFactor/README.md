# What's this?
These scripts can be used to estimate the data-driven fake factor.

* CreatFakesPlots.sh
  * This is the main script to create distributions for the data-driven fake estimation.
  * Runs on BlueTuesday/SR\_NoSys, CR\_NoSys, InvIso\_SR, InvIso\_CR
