#!/bin/sh


region_name=("isolation_SR" "isolation_CR" "invIsol_SR" "invIsol_CR")

#ls FinalPlots.isolation_SR* | xargs hadd merged.isolation_SR.root

#for index in `seq 0 3`
#do 
#  ls FinalPlots.${region_name[$index]}* | xargs hadd -f merged.${region_name[$index]}.root
#done

hadd -f merged.fakes.root merged.isolation_SR.root merged.isolation_CR.root merged.invIsol_SR.root merged.invIsol_CR.root
