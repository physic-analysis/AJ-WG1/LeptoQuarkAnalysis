#!/bin/sh


BEAMER_PATH="/eos/user/k/ktakeda/Leptoquark/"
VARS_LOG=("tag2pjet_0ptv_sT_Log.png"\
      "tag2pjet_0ptv_dRLepJet_Log.png"\
      "tag2pjet_0ptv_METCent_Log.png"\
      "tag2pjet_0ptv_PtTau_Log.png"\
      "tag2pjet_0ptv_MTauJet_Log.png"\
      "tag2pjet_0ptv_MLepJet_Log.png"\
      "tag2pjet_0ptv_dPhiLep0MET_Log.png")

VARS=("tag2pjet_0ptv_sT.png"\
      "tag2pjet_0ptv_dRLepJet.png"\
      "tag2pjet_0ptv_METCent.png"\
      "tag2pjet_0ptv_PtTau.png"\
      "tag2pjet_0ptv_MTauJet.png"\
      "tag2pjet_0ptv_MLepJet.png"\
      "tag2pjet_0ptv_dPhiLep0MET.png")

for Nb in `seq 0 2`
do
  for index in `seq 0 6`
  do
    cp ${BEAMER_PATH}/plots_lq_sr/LQ3900/BDTVarsPreselection/${Nb}/C_${Nb}${VARS_LOG[$index]} ${BEAMER_PATH}/Summary/LqBeammer/EventSelection/MyBdtInputVar/${Nb}
    cp ${BEAMER_PATH}/plots_lq_sr/LQ3900/BDTVarsPreselection/${Nb}/C_${Nb}${VARS[$index]} ${BEAMER_PATH}/Summary/LqBeammer/EventSelection/MyBdtInputVar/${Nb}
  done
done

FAKES=(\
  "CR_All_Preselection_Np"\
  "HighMtCR_All_Preselection_Np"\
  )

FAKES_MOD=(\
  "CR_All_Preselection_Np10tag_TauPt_log.png"\
  "CR_All_Preselection_Np11tag_TauPt_log.png"\
  "CR_All_Preselection_Np12tag_TauPt_log.png"\
  "CR_All_Preselection_Np30tag_TauPt_log.png"\
  "CR_All_Preselection_Np31tag_TauPt_log.png"\
  "CR_All_Preselection_Np32tag_TauPt_log.png"\
  )

for index in `seq 0 1`
do
  for np in 1 3
  do
    for nb in `seq 0 2`
    do
      #  cp /eos/user/k/ktakeda/Leptoquark/Fakes_rQCDv2/${FAKES[$index]} /eos/user/k/ktakeda/Leptoquark/Summary/LqBeammer/Background/FakeFactor/NumDenoPlots/${FAKES_MOD[$index]}
      cp /eos/user/k/ktakeda/Leptoquark/Fakes_rQCDv2/${FAKES[$index]}${np}${nb}tag.TauPt.log.png /eos/user/k/ktakeda/Leptoquark/Summary/LqBeammer/Background/FakeFactor/NumDenoPlots/${FAKES[$index]}${np}${nb}tag_TauPt_log.png
    done
  done
done
