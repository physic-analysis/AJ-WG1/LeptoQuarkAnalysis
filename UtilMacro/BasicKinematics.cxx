
void BasicKinematics()
{
  TFile * file = new TFile("/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/run/test/FinalPlots.TauLH.LQBeta05.aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta05_M900.0.root");
  TFile * fileM500 = new TFile("/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/run/test/FinalPlots.TauLH.LQBeta05.aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta05_M500.0.root");
  TFile * file_ttbar0 = new TFile("/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/run/test/FinalPlots.TauLH.ttbar.410470.0.root");
  TFile * file_ttbar1 = new TFile("/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/run/test/FinalPlots.TauLH.ttbar.410471.0.root");

  std::vector<std::vector<string>> vecPlots = {
    {"NJet"                  ,""      , "20", "The number of jets"},
    {"sTNoMET"               ," [GeV]", "20", "sT without Missing Et"},
    {"Ht"                    ," [GeV]", "1" , "MET.Pt() + All of the central Jet pt + lepton Pt"},
    {"sT"                    ," [GeV]", "20", "MET.Pt() + 1st & 2nd Jet pt + 1st lepton pt"},
    {"LeadingLepPT"          ," [GeV]", "10", "Leading lepton pT"},
    {"LeadingLepEta"         ,""      , "20", "Leading lepton #eta"},
    {"SubLeadingLepPt"       ," [GeV]", "20", "Sub-leading lepton pT"},
    {"SubLeadingLetEta"      ,""      , "20", "Sub-leading lepton #eta"},
    {"LeadingJetPT"          ," [GeV]", "20", "Leading jet pT"},
    {"LeadintJetEta"         ,""      , "20", "Leading jet #eta"},
    {"SubLeadingJetPT"       ," [GeV]", "20", "Sub-leading pT"},
    {"SubLeadintJetEta"      ,""      , "20", "Sub-leading #eta"},
    {"PtTau"                 ," [GeV]", "40", "Leading #tau p_T"},
    {"PairedTauJetSystemPt"  ," [GeV]", "10", "Paired #tau & jet (= LQ) p_T"},
    {"PairedLepJetSystemPt"  ," [GeV]", "10", "Paired lep  & jet (= LQ) p_T"},
    {"PairedTauJetSystemMass"," [GeV]", "10", "Paired #tau & jet (= LQ) mass"},
    {"PairedLepJetSystemMass"," [GeV]", "10", "Paired lep  & jet (= LQ) mass"},
    {"MET"                   ," [GeV]", "5" , "Missing ET"},
    {"METEta"                ,""      , "5" , "Missing ET #eta"},
    {"METPhi"                ,""      , "5" , "Missing ET #phi"},
    {"mMMC_DiTauMass"        ," [GeV]", "20", "Di-tau invariant mass"},
    {"mlljj"                 ," [GeV]", "5", ""},
    {"mjj"                   ," [GeV]", "5", "Two jets invariant mass"},
    {"MLQMin"                ," [GeV]", "5" , "min{m_LQ1, m_LQ2}"},
    {"MLQMax"                ," [GeV]", "5" , "max{m_LQ1, m_LQ2}"},
    {"MLQAve"                ," [GeV]", "5" , "(m_LQ1 + m_LQ2)/2"},
    {"MtW"                   ,""      , "20", ""},
    {"dPhijj"                ,""      , "5" , "#Delta #phi (jet, jet)"}, 
    {"dEtajj"                ,""      , "5" , "#Delta #eta (jet, jet)"}, 
    {"dRJ0Lep0"              ,""      , "5" , "#Delta R (1st jet, 1st lep)"}, 
    {"dRJ1Lep0"              ,""      , "5" , "#Delta R (2nd jet, 1st lep)"}, 
    {"dPhiJ0Lep0"            ,""      , "5" , "#Delta #phi (1st jet, 1st lep)"}, 
    {"dPhiJ1Lep0"            ,""      , "5" , "#Delta #phi (2nd jet, 1st lep)"}, 
    {"dEtaLQLQ"              ,""      , "5" , "#Delta #eta (LQ, LQ)"},
    {"dPhiLQLQ"              ,""      , "5" , "#Delta #phi (LQ, LQ)"},
    {"dRTauJet"              ,""      , "5" , "#Delta R between paired #tau and jet"},
    {"dRLepJet"              ,""      , "5" , "#Delta R between paired lep and jet"},
    {"dRLepTau"              ,""      , "20", "#Delta #phi between paired #tau and jet"},
    {"dPhiLepTau"            ,""      , "20", "#Delta #phi between paired lep and jet"},
    {"dRTauClosestJet"       ,""      , "20", ""},
    {"dPhiLep0MET"           ,""      , "5" , ""},
    {"dPtLepTau"             ," [GeV]", "5" , "#tau p_T - lepton p_T"},
    {"DRLepTau"              ,""      , "20", "#Delta R between lep and #tau"},
    {"METCentrality"         ,""      , "20", ""},
    {"LeadingTauPt"          ," [GeV]", "5" , ""},
    {"DPhiLepMET"            ," [GeV]", "20", ""},
    {"MTauJet"               ," [GeV]", "10", ""},
    {"MLepJet"               ," [GeV]", "10", ""}
  };
 
  std::vector<TH1D*> vecHist;
  for ( int iHist=0; iHist< vecPlots.size(); iHist++){
    vecHist.push_back((TH1D*)file->Get( Form("BasicKinematics/LQ3Up900_2tag2pjet_0ptv_%s",vecPlots[iHist][0].c_str())));
    TH1D* lqM500 = (TH1D*)fileM500->Get(Form("BasicKinematics/LQ3Up500_2tag2pjet_0ptv_%s",vecPlots[iHist][0].c_str()));

    TH1D* ttbar = (TH1D*)file_ttbar0->Get(Form("BasicKinematics/ttbar_2tag2pjet_0ptv_%s",vecPlots[iHist][0].c_str()));
    TH1D* ttbar1 = (TH1D*)file_ttbar1->Get(Form("BasicKinematics/ttbar_2tag2pjet_0ptv_%s",vecPlots[iHist][0].c_str()));
    ttbar->Add(ttbar1);

    TCanvas* c = new TCanvas("c","c");
    int bin = atoi(vecPlots[iHist][2].c_str());
    std::cout << bin << std::endl;
    vecHist[iHist]->Rebin(bin);
    ttbar->Rebin(bin);
    lqM500->Rebin(bin);

    vecHist[iHist]->SetLineColor(kMagenta);
    lqM500->SetLineColor(kBlue);
    vecHist[iHist]->GetXaxis()->SetTitle(Form("%s %s", vecPlots[iHist][0].c_str() ,vecPlots[iHist][1].c_str()));
    ttbar->GetXaxis()->SetTitle(Form("%s %s", vecPlots[iHist][0].c_str() ,vecPlots[iHist][1].c_str()));
    
    vecHist[iHist]->Scale(1/vecHist[iHist]->Integral());
    ttbar->Scale(1/ttbar->Integral());
    lqM500->Scale(1/lqM500->Integral());



    ttbar->Draw("hist same");
    lqM500->Draw("hist same");
    vecHist[iHist]->Draw("hist same");
    
    
    if ( vecHist[iHist]->GetBinContent(vecHist[iHist]->GetMaximumBin()) > ttbar->GetBinContent(ttbar->GetMaximumBin() )){
      vecHist[iHist]->Draw("hist");
      ttbar->Draw("hist same");
    lqM500->Draw("hist same");
    }
    
    TLatex latex;
    latex.SetNDC(1);
    latex.DrawLatex(0.25, 0.96, Form("%s", vecPlots[iHist][3].c_str()));
    c->SaveAs( Form("C_LQ3Up900_%s.pdf", vecPlots[iHist][0].c_str()));
  }
}
