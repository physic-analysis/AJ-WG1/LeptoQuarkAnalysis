
print("\\begin{frame}{}") 
print("\\begin{table}[]") 
print("\\tiny")
print("\\begin{tabular}{ll|cc}")
print("\\multicolumn{2}{c|}{\multirow{2}{*}{Sample}} & \multicolumn{2}{c}{Lumi-weighted yields} \\\\ ")
print("\\multicolumn{2}{c|}{} & 1-tag & 2-tag \\\\ \\hline\\hline")

yield_table={}
with open("yield_1tag2pjet_0ptv.txt","r") as f:
    for line in f:
        words= line.split()
        if words[0] not in yield_table:
            yield_table[words[0]]=[]
        yield_table[words[0]].append(words[2])

with open("yield_2tag2pjet_0ptv.txt","r") as f:
    for line in f:
        words= line.split()
        if words[0] not in yield_table:
            yield_table[words[0]]=[]
        yield_table[words[0]].append(words[2])

for sample, value in yield_table.items():
    if sample == "ttbar": 
        print("\multicolumn{2}{l|}{$t\\bar{t}$}  & \multicolumn{1}{c|}{" + value[0] + "} & " + value[1] + " \\\\ \\hline")

for sample, value in yield_table.items():
    if sample == "stopWt": 
        print("\multicolumn{2}{l|}{single top $(W+t)$}  & \multicolumn{1}{c|}{" + value[0] + "} & " + value[1] + " \\\\ \\hline")

for sample, value in yield_table.items():
    if sample == "stopt": 
        print("\multicolumn{2}{l|}{single top (t-channel)}  & \multicolumn{1}{c|}{" + value[0] + "} & " + value[1] + " \\\\ \\hline")

for sample, value in yield_table.items():
    if sample == "stops": 
        print("\multicolumn{2}{l|}{single top (s-channel)}  & \multicolumn{1}{c|}{" + value[0] + "} & " + value[1] + " \\\\ \\hline")

for sample, value in yield_table.items():
    if sample == "WZ": 
        print("\multicolumn{2}{l|}{WZ}  & \multicolumn{1}{c|}{" + value[0] + "} & " + value[1] + " \\\\ \\hline")

for sample, value in yield_table.items():
    if sample == "WW": 
        print("\multicolumn{2}{l|}{WW}  & \multicolumn{1}{c|}{" + value[0] + "} & " + value[1] + " \\\\ \\hline")

for sample, value in yield_table.items():
    if sample == "ZZ": 
        print("\multicolumn{2}{l|}{ZZ}  & \multicolumn{1}{c|}{" + value[0] + "} & " + value[1] + " \\\\ \\hline")

for sample, value in yield_table.items():
    if sample == "ttH": 
        print("\multicolumn{2}{l|}{ttH}  & \multicolumn{1}{c|}{" + value[0] + "} & " + value[1] + " \\\\ \\hline")

for sample, value in yield_table.items():
    if sample == "DY": 
        print("\multicolumn{2}{l|}{Drel-Yan}  & \multicolumn{1}{c|}{" + value[0] + "} & " + value[1] + " \\\\ \\hline")

for sample, value in yield_table.items():
    if sample == "DYtt": 
        print("\multicolumn{2}{l|}{Drel-Yan\\to\\tau\\tau}  & \multicolumn{1}{c|}{" + value[0] + "} & " + value[1] + " \\\\ \\hline")

for sample, value in yield_table.items():
    if sample == "VH": 
        print("\multicolumn{2}{l|}{VH}  & \multicolumn{1}{c|}{" + value[0] + "} & " + value[1] + " \\\\ \\hline")

for sample, value in yield_table.items():
    if sample == "Fake": 
        print("\multicolumn{2}{l|}{Fake}  & \multicolumn{1}{c|}{" + value[0] + "} & " + value[1] + " \\\\ \\hline")

for sample, value in yield_table.items():
    if sample == "Zttbb": 
        print("\multicolumn{1}{l|}{\multirow{3}{*}{$Z\\to\\tau\\tau$}} & $bb$       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\cline{2-4}")

for sample, value in yield_table.items():
    if sample == "Zttbc": 
        print("\multicolumn{1}{l|}{} & $bc$       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\cline{2-4}")

for sample, value in yield_table.items():
    if sample == "Zttbl": 
        print("\multicolumn{1}{l|}{} & $b\\ell$       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\cline{2-4}")

for sample, value in yield_table.items():
    if sample == "Zttcc": 
        print("\multicolumn{1}{l|}{} & $cc$       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\cline{2-4}")

for sample, value in yield_table.items():
    if sample == "Zttcl": 
        print("\multicolumn{1}{l|}{} & $c\\ell$       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\cline{2-4}")

for sample, value in yield_table.items():
    if sample == "Zttl": 
        print("\multicolumn{1}{l|}{} & $\\ell\\ell$       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\hline")

for sample, value in yield_table.items():
    if sample == "Zbb": 
        print("\multicolumn{1}{l|}{\multirow{3}{*}{$Z$}} & $bb$       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\cline{2-4}")

for sample, value in yield_table.items():
    if sample == "Zbc": 
        print("\multicolumn{1}{l|}{} & $bc$       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\cline{2-4}")

for sample, value in yield_table.items():
    if sample == "Zbl": 
        print("\multicolumn{1}{l|}{} & $b\\ell$       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\cline{2-4}")

for sample, value in yield_table.items():
    if sample == "Zcc": 
        print("\multicolumn{1}{l|}{} & $cc$       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\cline{2-4}")

for sample, value in yield_table.items():
    if sample == "Zcl": 
        print("\multicolumn{1}{l|}{} & $c\\ell$       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\cline{2-4}")

for sample, value in yield_table.items():
    if sample == "Zl": 
        print("\multicolumn{1}{l|}{} & $\\ell\\ell$       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\hline")

for sample, value in yield_table.items():
    if sample == "W": 
        print("\multicolumn{1}{l|}{\\multirow{3}{*}{$W+$jets}}       & $e/\\mu$       & \multicolumn{1}{c|}{" + value[0] + "}  & " + value[1] + "  \\\\ \\cline{2-4}")

for sample, value in yield_table.items():
    if sample == "Wtt": 
        print("\multicolumn{1}{l|}{} & $\\tau\\nu_{\\tau}$       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\hline\\hline")

for sample, value in yield_table.items():
    if sample == "bkg:": 
        print("\multicolumn{2}{l|}{Total background}       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\hline\\hline")

for sample, value in yield_table.items():
    if sample == "data": 
        print("\multicolumn{2}{l|}{Data}       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\hline\\hline")

for sample, value in yield_table.items():
    if sample == "LQ3Up900": 
        print("\multicolumn{2}{l|}{LQUp3900}       & \multicolumn{1}{c|}{"+ value[0]+ "}  & " + value[1] + " \\\\ \\hline")



print("\end{tabular}")
print("\end{table}")
print("\end{frame}")
