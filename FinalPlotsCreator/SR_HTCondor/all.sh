#!/bin/sh

SAMPLE_MC16a=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/AtTheDriveIn/
SAMPLE_MC16d_SR=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/BlueTuesday/SR_NoSys
SAMPLE_MC16d_LQ=/eos/atlas/unpledged/group-tokyo/users/ktakeda/Data/Leptoquark/LepHad/LQSignalSample/
SAMPLE_MC16d_AntiTau=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/BlueTuesday/CR_NoSys

SAMPLE=""
if [ $1 = "163" ]; then
  SAMPLE=${SAMPLE_MC16d_LQ}
else
  SAMPLE=${SAMPLE_MC16d_SR}
fi

FinalHHbbtautauLH \
  --release 31\
  -n -1\
  -s 0 \
  -d ${SAMPLE}\
  -p $1 \
  -q 0 \
  -y 2017 \
  --analysisType TauLH \
  --outputName FinalPlots.root \
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1\
  --fake 0 \
  --antiTau 0 \
  --ff 0 \
  --lq3 0\
  --lq 1\
  --outputNtupName testNtup.root
