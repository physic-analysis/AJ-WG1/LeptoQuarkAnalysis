#!/bin/sh

SAMPLE_MC16d_SR=/data/data2/zp/ktakeda/BlueTuesday/SR_NoSys
SAMPLE_MC16d_LQ=/data/data2/zp/ktakeda/BlueTuesday/LQSignalSample/
SAMPLE_MC16d_AntiTau=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/BlueTuesday/CR_NoSys

SAMPLE=""
if [ $1 = "163" ]; then
  SAMPLE=${SAMPLE_MC16d_LQ}
else
  SAMPLE=${SAMPLE_MC16d_SR}
fi

# setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
echo ">>> setupATLAS done."

CurrentDir=${PWD}
cd /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/build_slc6/
asetup AnalysisBase,21.2.60,here,slc6
echo ">>> asetup done."

source x*/setup.sh 
echo ">>> source x*/setup.sh done."
cd ${CurrentDir}

FinalHHbbtautauLH \
  --release 31\
  -n -1\
  -s 0 \
  -d ${SAMPLE}\
  -p $1 \
  -q 0 \
  -y 2017 \
  --analysisType TauLH \
  --outputName FinalPlots.root \
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1\
  --fake 0 \
  --antiTau 0 \
  --ff 0 \
  --lq3 0\
  --lq 1\
  --outputNtupName testNtup.root
