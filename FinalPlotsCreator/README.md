# How to create each FinalPlot file
These scripts can be used to create FinalPlots files. The FinalPlots files will be used by the MIA/python/plot2Tau.py to see each variable distribution.
If you do that, please see the FinalPlotsReader/README.md.

## Use scripts under the SR/ directory
When you create the FinalPlots, you use the FinalHHbbtautauLH command. But this command needs the correct options.
This is because I created the wrapper scripts under the SR directory.
  * data.sh    : Run on data samples
  * ttbar.sh   : Run on ttbar samples. This scripts can output ttbar Fake samples simultaneously.
  * SMHiggs.sh : Run on higgs samples. 
  * others.sh  : Run on the other background samples. For the future, this others scripts should be separated per each sample number.

## Ditails of the options
Those wrapper scripts can handle the options correctly. Thus if it is needed to modify the script, you should be carefull. 
If you couldn't modify the code correctly, the script may not work fine and cause unknown problems.
  * lq : Do the leptoquark (lep-had) dedicated workflow.
  * lq3 : Do the leptoquark (had-had) dedicated workflow.
## Known problmes on BlueTuesday
The following samples doesn't exist on BlueTuesday production.
  * SMHiggs num=161      
  * MG (Madgraph) 
  * ZqqZvv  num=5

## Merge root files
When you finish to create the FinalPlots, you should merge these files into merge.root.
* ls | grep FinalPlots | xargs hadd merged.root

# Use HTCondor 
You can also run this script via HTCondor. You can see the SR_HTCondor directory.
```
condor_submit Condor.sub (for nominal)
condor_submit Condor_fakes.sub (for data-driven fake estimation)
```
