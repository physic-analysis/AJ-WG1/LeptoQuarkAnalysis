#!/bin/sh
# 
# n events (default -1)
# --release (default 31)
# s nsysts
# p sample number
# q splitJobsSyst
# t bTaggingTruthTag
# y sample year 
# splitTauFakes 
# ff 
#
SAMPLE_MC16a=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/AtTheDriveIn/
SAMPLE_MC16d=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/BlueTuesday/SR_NoSys

SAMPLE=${SAMPLE_MC16d}

SAMPLE_NUMBER=1
FinalHHbbtautauLH \
  --release 31\
  -n -1\
  -s 0 \
  -d ${SAMPLE}\
  -p ${SAMPLE_NUMBER} \
  -q 0 \
  -y 2017 \
  --analysisType TauLH \
  --outputName FinalPlots.root \
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 0\
  --fake 0 \
  --antiTau 0 \
  --ff 1 \
  --lq3 0\
  --lq 1\
  --outputNtupName testNtup.root
