#!/bin/sh
# 
# n events (default -1)
# --release (default 31)
# s nsysts
# p sample number
# q splitJobsSyst
# t bTaggingTruthTag
# y sample year 
# splitTauFakes 
# ff 
#

SAMPLE="/eos/atlas/unpledged/group-tokyo/users/ktakeda/Data/Leptoquark/LepHad/LQSignalSample/"

SAMPLE_NUMBER=163
FinalHHbbtautauLH \
  --release 31\
  -n -1\
  -s 0 \
  -d ${SAMPLE}\
  -p ${SAMPLE_NUMBER} \
  -q 0 \
  -y 2017 \
  --analysisType TauLH \
  --outputName FinalPlots.root \
  --pileupReweightingFromNtuple 1  \
  --splitTauFakes 1\
  --fake 0 \
  --antiTau 0 \
  --ff 0 \
  --lq3 0\
  --lq 1\
  --cutFlow 0\
  --outputNtupName testNtup.root
