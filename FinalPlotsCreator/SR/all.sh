#!/bin/sh

DIR=/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/LeptoQuarkAnalysis/FinalPlotsCreator/SR

. ${DIR}/ttbar.sh
. ${DIR}/SMHiggs.sh
. ${DIR}/others.sh
. ${DIR}/data.sh
