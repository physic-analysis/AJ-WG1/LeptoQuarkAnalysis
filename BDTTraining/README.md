# How to run the BDT
## Copy the testNtup root files
At first, you should copy the following testNtup root files into the BdtInput directory.
You can use the CreateBdtInput.sh script to copy them, but if you need to do by yourself, please be sure that 
if the copy files are correct or not.

* testNtup.TauLH.ttbar.410470.0.root
* testNtup.TauLH.ttbar.410471.0.root 
* testNtup.TauLH.LQBeta05.aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta05_M500.0.root
* testNtup.TauLH.LQBeta05.aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta05_M900.0.root

## Get the BDT score
The following description is to use the BDT algorithm. Before dogin this training, you have to prepare the testNtup files. This file could be outputted when you run the MIA to make FinalPlots.
The testNtup has various variables which will be used by the BDT algorithm. If you don't have your disired variables, first you have to re-create the testNtup root file.
When these preparation has been completed, you can run the BDT algorithm!

1. source setup_bdt.sh
2. . CreateBdtInput.sh
  * Copy the necessarily files into the BdtInput directory.
  * TreeConvertor.cxx : The testNtup files has one branch and many leaves. Thus to make it easy to handle the datam, this script will convert many branches and one leaf structure.
3. . ExecTrain.sh
  * This wraps 'root -l TMVAClassification.C'
  * This is a wrapper script of the TMVA.
  * The output files will be created under the output directory.

The above procedure can serve you the BDT output score distributions, etc. You can see the distribuition by the follwoing command.

To run the TMVA, please execute the following commands.
* root -l
* TMVA::TMVAGui("./output/PleasePutYourRootFileName.root")

## Re-create the FinalPlots with the new BDT score
After checking the score, you should re-create the FinalPlots files to evaluate the mass limit with this new BDT score.
The BDT results are recoded as the .xml files, and you should tell the MIA/Root/HbbMVA.cxx the path to these xml files.
If you don't want to do so, you can put the new xml files to the HbbMVA/data directory with the correct file name.
As you see the HbbMVA.cxx, you can find we need two xml files. 

* Put these xml files to source/HbbMVA/data/
* Edit the source/MIA/Root/HbbMVA.cxx 

After modifying the HbbMVA.cxx, you can run the MIA.

* CreateFinalPlotsWithNewBdt.sh

After that, you can get new results with the new BDT. You can go ahead to calculate the mass limist with WSMaker.
