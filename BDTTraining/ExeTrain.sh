#!/bin/sh

<<COMMENT_OUT
rm ./error.txt
rm ./success.txt
for iSwitch in "A" "B";
do
  for iMassPoint in 500 900 1300 1700;
  do
    for iBtag in 1 2;
      do
      root -l -q 'TMVAClassification.C("'${iSwitch}'", "'${iMassPoint}'", "'${iBtag}'")'
      if [ $? -gt 0 ];then
        echo "root -l -q 'TMVAClassification.C(""'${iSwitch}'"", ""'${iMassPoint}'"", ""'${iBtag}'"")'" >> error.txt
      else 
        echo "root -l -q 'TMVAClassification.C(""'${iSwitch}'"", ""'${iMassPoint}'"", ""'${iBtag}'"")'" >> success.txt
      fi
    done
  done
done
COMMENT_OUT

#root -l -b -q 'TMVAClassification.C("A", "500", "1")'
#root -l -b -q 'TMVAClassification.C("B", "500", "1")'
#root -l -b -q 'TMVAClassification.C("A", "500", "2")'
#root -l -b -q 'TMVAClassification.C("B", "500", "2")'
#
#root -l -b -q 'TMVAClassification.C("A", "900", "1")'
#root -l -b -q 'TMVAClassification.C("B", "900", "1")'
#root -l -b -q 'TMVAClassification.C("A", "900", "2")'
#root -l -b -q 'TMVAClassification.C("B", "900", "2")'
#
#root -l -b -q 'TMVAClassification.C("A", "1300", "1")'
#root -l -b -q 'TMVAClassification.C("B", "1300", "1")'
root -l -b -q 'TMVAClassification.C("A", "1300", "2")'
#root -l -b -q 'TMVAClassification.C("B", "1300", "2")'
#
#root -l -b -q 'TMVAClassification.C("A", "1700", "1")'
#root -l -b -q 'TMVAClassification.C("B", "1700", "1")'
#root -l -b -q 'TMVAClassification.C("A", "1700", "2")'
#root -l -b -q 'TMVAClassification.C("B", "1700", "2")'
