
void TreeConvertor(TString tree_name, TString cutflow, TString input_file) {
  
  TFile* file = new TFile("./BdtInput/" + cutflow + "/" + input_file);
  TTree* tree = (TTree*)file->Get( tree_name );

  std::string variable_name[51] ={
    "dEtaLQLQ",
    "dPhiLQLQ",
    "dRTauJet",
    "dRLepJet",
    "PtTauJet",
    "PtLepJet",
    "PtTauJetCorr",
    "PtLepJetCorr",
    "PtTau",
    "PtB1",
    "PtB2",
    "dPtLepTau",
    "MET",
    "MTW",
    "METCent",
    "dPhiLep0MET",
    "Ht",
    "sT",
    "sTNoMET",
    "MTauJet",
    "MLepJet",
    "MTauJetCorr",
    "MLepJetCorr",
    "MLQMin",
    "MLQMax",
    "MLQAve",
    "MLQSum",
    "Mjj",
    "Mlljj",
    "mMMC",
    "dEtajj",
    "dPhijj",
    "dEtall",
    "dEtajj",
    "nSigJet",
    "nForwardJet",
    "ptOrdered",
    "BTag",
    "evtFlav",
    "weight",
    "isOdd",
    "fatJet",
    "isFakeTau",
    "dRLepJetpdr",
    "MTauJetpdr",
    "MLepJetpdr",
    "MLQSumpdr",
    "dRLepJetdpt",
    "MTauJetdpt",
    "MLepJetdpt",
    "MLQSumdpt"
  };
  double variable[51];
  TLeaf* leaf[51];

  TTree* new_tree_test  = new TTree("Nominal","test sample");
  TTree* new_tree_train = new TTree("Nominal","train sample");
  int nEntry = tree->GetEntries();

  for (int iVar=0; iVar<51; iVar++){
    variable[iVar] = 0;
    new_tree_test ->Branch( Form("%s", variable_name[iVar].c_str()), &variable[iVar], Form("%s/D", variable_name[iVar].c_str()));
    new_tree_train->Branch( Form("%s", variable_name[iVar].c_str()), &variable[iVar], Form("%s/D", variable_name[iVar].c_str()));
    leaf[iVar] = tree->GetLeaf("MVA", Form("%s", variable_name[iVar].c_str()));
  }

  for ( int iEntry=0; iEntry<nEntry; iEntry++){
    if ( iEntry%2 == 0 ) {
      tree->GetEntry(iEntry);
      for (int iVar=0; iVar<51; iVar++){
        variable[iVar] = leaf[iVar]->GetValue();
      }
      new_tree_test->Fill();
    } else {
      tree->GetEntry(iEntry);
      for (int iVar=0; iVar<51; iVar++){
        variable[iVar] = leaf[iVar]->GetValue();
      }
      new_tree_train->Fill();
    }
  }

  TFile* newfile_test = new TFile("./BdtInput/" + cutflow + "/" + tree_name + "_test.root","recreate");
  new_tree_test->Write();
  newfile_test ->Close();
  
  TFile* newfile_train = new TFile("./BdtInput/" + cutflow + "/"  + tree_name + "_train.root","recreate");
  new_tree_train->Write();
  newfile_train ->Close();
}
