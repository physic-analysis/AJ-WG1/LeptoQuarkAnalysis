#!/bin/sh

CUTFLOW=$1
DIR="/eos/atlas/unpledged/group-tokyo/users/ktakeda/Data/Leptoquark/LepHad/BlueTuesday/"
BACK_GROUND="${DIR}/Nominal/${CUTFLOW}/testNtup"
LQ_SIGNAL="${DIR}/LQSample/${CUTFLOW}/testNtup"

echo "--------------------------------------"
echo "   Start to prepare the BDT training  "
echo "--------------------------------------"
echo $DIR
echo $BACK_GROUND
echo $LQ_SIGNAL

echo "(1) ttbar backgrund sample will be copied ..."
hadd -f ./BdtInput/${CUTFLOW}/testNtup.TauLH.ttbar.${CUTFLOW}.root ${BACK_GROUND}/testNtup.TauLH.ttbar.410470.0.root ${BACK_GROUND}/testNtup.TauLH.ttbar.410471.0.root 

echo "(2) Leptoquark samples will be copied ..."
cp ${LQ_SIGNAL}/testNtup.TauLH.LQBeta05.aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta05_M500.0.root ./BdtInput/${CUTFLOW}/
cp ${LQ_SIGNAL}/testNtup.TauLH.LQBeta05.aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta05_M900.0.root ./BdtInput/${CUTFLOW}/
cp ${LQ_SIGNAL}/testNtup.TauLH.LQBeta05.aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta05_M1300.0.root ./BdtInput/${CUTFLOW}/
cp ${LQ_SIGNAL}/testNtup.TauLH.LQBeta05.aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta05_M1700.0.root ./BdtInput/${CUTFLOW}/

echo "All of the input sample preparation is done!!"
echo "Secondly, convert these files to be readable."

root -l -q 'TreeConvertor.cxx("ttbar"    , "'${CUTFLOW}'", "testNtup.TauLH.ttbar.'"${CUTFLOW}"'.root")'
root -l -q 'TreeConvertor.cxx("LQ3Up500" , "'${CUTFLOW}'", "testNtup.TauLH.LQBeta05.aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta05_M500.0.root")'
root -l -q 'TreeConvertor.cxx("LQ3Up900" , "'${CUTFLOW}'", "testNtup.TauLH.LQBeta05.aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta05_M900.0.root")'
root -l -q 'TreeConvertor.cxx("LQ3Up1300", "'${CUTFLOW}'", "testNtup.TauLH.LQBeta05.aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta05_M1300.0.root")'
root -l -q 'TreeConvertor.cxx("LQ3Up1700", "'${CUTFLOW}'", "testNtup.TauLH.LQBeta05.aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta05_M1700.0.root")'
