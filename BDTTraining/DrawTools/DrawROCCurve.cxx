
void DrawROCCurve()
{
    std::vector<TString> file_name = {
        "LQ300_BDT.NewCutFlow_1btag.weightA.root",
        "LQ500_BDT.NewCutFlow_1btag.weightA.root",
        "LQ900_BDT.NewCutFlow_1btag.weightA.root",
        "LQ1300_BDT.NewCutFlow_1btag.weightA.root",
        "LQ1700_BDT.NewCutFlow_1btag.weightA.root",
    };
    std::vector<TString> file_name_new = {
        "LQ300_BDT.NewCutFlow_1btag.weightA_NewVarSet.root",
        "LQ500_BDT.NewCutFlow_1btag.weightA_NewVarSet.root",
        "LQ900_BDT.NewCutFlow_1btag.weightA_NewVarSet.root",
        "LQ1300_BDT.NewCutFlow_1btag.weightA_NewVarSet.root",
        "LQ1700_BDT.NewCutFlow_1btag.weightA_NewVarSet.root",
    };
    
    TCanvas* c = new TCanvas("c","c");
    EColor ec[5] = {kBlack, kMagenta, kBlue, kGreen, kRed};
    for ( size_t iFile=0; iFile<file_name.size(); iFile++ ){
        TFile* file = new TFile( "output/" + file_name.at(iFile) );
        TH1D* hist = ((TH1D*)file->Get("Method_BDT/NewCutFlow_BDT_1tag_A/MVA_NewCutFlow_BDT_1tag_A_rejBvsS"));
        hist->SetLineColor(ec[iFile]);
        hist->SetLineStyle(2);
        hist->Draw("same");
        hist->GetYaxis()->SetRangeUser(0.95,1);
    }
    
    for ( size_t iFile=0; iFile<file_name.size(); iFile++ ){
        TFile* file = new TFile( "output/" + file_name_new.at(iFile) );
        TH1D* hist = ((TH1D*)file->Get("Method_BDT/NewCutFlow_BDT_1tag_A/MVA_NewCutFlow_BDT_1tag_A_rejBvsS"));
        hist->SetLineColor(ec[iFile]);
        hist->SetLineStyle(1);
        hist->Draw("same");
        hist->GetYaxis()->SetRangeUser(0.95,1);
    }
}
