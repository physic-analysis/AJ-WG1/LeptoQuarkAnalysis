
void DrawCorrelation()
{
    std::vector<TString> file_name = {
        "LQ300_BDT.NewCutFlow_1btag.weightA.root",
        "LQ500_BDT.NewCutFlow_1btag.weightA.root",
        "LQ900_BDT.NewCutFlow_1btag.weightA.root",
        "LQ1300_BDT.NewCutFlow_1btag.weightA.root",
        "LQ1700_BDT.NewCutFlow_1btag.weightA.root",
    };
    std::vector<TString> file_name_new = {
        "LQ300_BDT.NewCutFlow_1btag.weightA_NewVarSet.root",
        "LQ500_BDT.NewCutFlow_1btag.weightA_NewVarSet.root",
        "LQ900_BDT.NewCutFlow_1btag.weightA_NewVarSet.root",
        "LQ1300_BDT.NewCutFlow_1btag.weightA_NewVarSet.root",
        "LQ1700_BDT.NewCutFlow_1btag.weightA_NewVarSet.root",
    };
    
    gStyle->SetPalette(1);
    TCanvas* c = new TCanvas("c","c", 1500, 1000);
    c->Divide(3,2);
    c->SaveAs("CorrelationPlots.pdf[");
    EColor ec[5] = {kBlack, kMagenta, kBlue, kGreen, kRed};
    for ( size_t iFile=0; iFile<file_name.size(); iFile++ ){
        TFile* file = new TFile( "output/" + file_name.at(iFile) );
        TH1D* histS = ((TH1D*)file->Get("CorrelationMatrixS"));
        //histS->SetMarkerColor(kWhite);
        histS->SetMarkerSize(2);
        c->cd(iFile+1);
        histS->Draw("colz text");
    }
    c->SaveAs("CorrelationPlots.pdf");

    for ( size_t iFile=0; iFile<file_name.size(); iFile++ ){
        TFile* file = new TFile( "output/" + file_name_new.at(iFile) );
        TH1D* histS = ((TH1D*)file->Get("CorrelationMatrixS"));
        //histS->SetMarkerColor(kWhite);
        histS->SetMarkerSize(2);
        c->cd(iFile+1);
        histS->Draw("colz text");
    }
    c->SaveAs("CorrelationPlots.pdf");
    c->SaveAs("CorrelationPlots.pdf]");
}
