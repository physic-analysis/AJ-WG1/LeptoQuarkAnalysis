# Analysis flow
1. FinalPlotsCreator
  * At first, you should skim the CxAOD files, e.g. BlueTuestay, into the FinalPlots root files.
    * input : CxAOD
    * outpt : FinalPlots.\*.root

2. FinalPlotsReader
  * After preparing the Finalplots, you can see the some basic kinematics using this package. You can do the comparison between data and MC. 
  If there are some discrepancy, you should check the lumionsity weight, fake factor, and so on. 
    * input : FinalPlots.\*.root
    * outpt : plots\_lq (A directory)

3. DataDrivenFakeFactor
  * You should estimate the data driven fake factor using this package. There are three steps.
  * (1) CreateFakesPlots.sh : Create data/MC FinalPlots for the estimation of the fake background
    * input : CxAOD
    * outpt : FinalPlots.fakes.\*.root
  * (2) MIA/python/FF.py  : Create a root file containing data-driven fake factor
    * input : FinalPlots.fakes.\*.root
    * outpt : Fakes/FF.data.root (A directory)
  * (3) RerunWithDataDrivenFF.sh :  Create the FinalPlots with the data driven fake factor.
    * input : CxAOD
    * outpt : FinalPlots.root

4. FinalPlotsCreator (for data-driven fakes)
  * After calculating the data-driven fake factors, you can estimate the data-driven fake backgrounds. Thus you should run the FinalPlotsCreator again.
    * input : CxAOD
    * outpt : FinalPlots.Fake.\*.root

5. BDT training

6. Limit setting

7. ReproduceHH
  * This is to reproduce the latest HH results.

# Discriptions of each tool
  * FinalPlotsCreator
    * To create FinalPlots\*.root files, which is the output files by MIA.
    * And this can be used to output the testNtup\*.root files for the BDT training.
  * FinalPlotsReader
    * After the preparation of the FinalPlots\*.root files, this Reader can generate each variable distributions. 
  * BDTTrainig
    * Use the testNtup\*.root, and output the BDT score distributions.

# How to write the MIA config file
## Variables for MC
* splitTauFakes (true/false) : MIA/Root/HbbEvent::IsFakeTau(int itau, int pdgid);
  * ttbar background can be divied into (1) real tau bkg and (2) fake tau bkg. If you use this option, you can divide ttbar MC samples.
    * See TauJet container with given an index (itau), and compare the object's pdg with an argument (pdgid).
    * If TauJet container truth match pdgId is not 15, the function returns true (= This index tau is not true tau).
