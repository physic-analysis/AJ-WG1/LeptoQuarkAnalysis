# https://its.cern.ch/jira/browse/ATLMCPROD-6343

import sys
import subprocess 

args = sys.argv

run_number = args[1]
beta       = args[2]
mass       = args[3]
tag        = args[4]

data = 'mc16_13TeV:mc16_13TeV.' + run_number + '.aMcAtNloPy8EG_OffDiag_LQu_ta_ld_0p3_beta_' + beta + '_hnd_1p0_M' + mass + '.deriv.DAOD_HIGG4D2*' + tag

print data 
subprocess.call(['rucio', 'list-dids', data])
#rucio download mc16_13TeV:mc16_13TeV.${run_number}.aMcAtNloPy8EG_OffDiag_LQu_ta_ld_0p3_beta_0p5_hnd_1p0_M900.deriv.DAOD_HIGG4D2.${tag}
