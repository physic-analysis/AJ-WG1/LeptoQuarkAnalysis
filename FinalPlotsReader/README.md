# How to read final plots
After creating final plots, you can see a lot of final plots. Thus at first, you should merged these files.

* source setup.sh
* source hadd.sh
* source exe.sh BDTVarsPreselection

After the exe.sh, you can see the plots\_lq directory which contains some histograms as .png style.

When you draw the data fakes, you should use the steer2Tau option, data\_fakes as True.
