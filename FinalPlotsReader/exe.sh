#!/bin/sh

VARS=$1
if [ "$VARS" == "" ]; then
  echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  echo "              E R R O R               "
  echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  echo "This exe.sh script is a wrapper script of the MIA/python/plot2TauLepHad.py."
  echo "You should choose the follwing variables, which the script will output."
  echo " - BDTVarsPreselection" 
  echo " - LQ"
  echo " "
  echo " ex. "
  echo "   source exe.sh BDTVarsPreselection"
  echo "Please give the correct argument"
else
  python python/plot2TauLepHad.py -s "$VARS" -m 900 --type "LQ3" --inname "merged.all" --outname "plots_lq"
fi
