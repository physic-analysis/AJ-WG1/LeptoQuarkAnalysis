#! /usr/bin/env python

import sys
import ROOT as R
R.PyConfig.IgnoreCommandLineOptions = True
from plotting import var
from plotting2Tau import Plotter2Tau as Plotter
from steer2Tau import load

def BDTVars(name, label, signal, draw_signal = True):
    p.addVarSet(name,
                    var("dEtaLQLQ"   , "#Delta#eta_{LQLQ}", 0, 5, 10),
                    var("dPhiLQLQ"   , "#Delta#phi_{LQLQ}", 0, 3.2, 2),                                         
                    var("dRTauJet"   , "#DeltaR_{#taujet}", 0, 5, 10),
                    var("dRLepJet"   , "#DeltaR_{lepjet}", 0, 5, 10),                                         
                    var("PtTauJet"   , "p_{T}^{#taujet}", 0, 400, 5),
                    var("PtLepJet"   , "p_{T}^{lepjet}", 0, 400, 5),				    
                    var("PtTau"      , "p_{T}^{#tau} [GeV]",  0, 400, 5),
                    var("PtB1"       , "p_{T}^{b_{1}} [GeV]", 0, 500, 2),
                    var("PtB2"       , "p_{T}^{b_{2}} [GeV]", 0, 500, 2),
                    var("dPtLepTau"  , "#Delta p_{T}(l, #tau)", -150, 200, 1),                     
                    var("MET"        , "E_{T}^{miss} [GeV]", 0, 400, 5),
                    var("dPhiLep0MET", "#Delta#phi(l, MET)", 0, R.TMath.Pi(), 2),
                    var("MtW"        , "M_{T}^{W} [GeV]", 0, 200, 4),
                    var("METCent"    , "MET #phi centrality", -1.5, 1.5, 1),
                    var("Ht"         , "Ht [GeV]", 0, 1000, 2),
                    var("sT"         , "s_{T} [GeV]", 0, 1200, 5),
                    var("MTauJet"    , "m_{#taujet} [GeV]", 0, 800, 4),
                    var("MLepJet"    , "m_{lepjet} [GeV]", 0, 800, 4),                
                    var("MLQMin"     , "m_{LQ} (min) [GeV]", 0, 800, 4),
                    var("MLQMax"     , "m_{LQ} (max) [GeV]", 0, 800, 4),
                    var("MLQAve"     , "m_{LQ} (ave) [GeV]", 0, 800, 4),                                
                    var("mlljj"      , "m_{lljj} [GeV]", 0, 1500, 20),
                    var("mjj"        , "m_{bb} [GeV]", 0, 500, 5),
                    var("mMMC"       , "m_{MMC} [GeV]", 0, 500,25),                
                    var("dPhijj"     , "#Delta#phi_{jj}", 0, R.TMath.Pi(), 2),
                    var("dPhill"     , "#Delta#phi_{ll}", 0, R.TMath.Pi(), 2),
                    var("dEtajj"     , "#Delta#eta_{jj}", 0, 5, 10),
                    var("dEtall"     , "#Delta#eta_{ll}", 0, 5, 10),                    
                    label = label, signal = draw_signal
                    )

    return

def wrapAddVarSet(signal):
    p.addVarSet("ControlLep",
                var("Muon0Pt", "p_{T}^{#mu} [GeV]",  0, 500 if p.signal == "LQ3" else 200, [5,5,5] if p.signal == "LQ3" else [2,5,5]), #, sysplots=["0tag2pjet_0ptv", "1tag2pjet_0ptv", "2tag2pjet_0ptv"]),
                var("Muon0Eta", "#eta^{#mu}", -3.5, 3.5, 5),
                ##var("Muon0Phi", "#phi^{#mu}", -R.TMath.Pi(), R.TMath.Pi(),2),
                var("Elec0Pt", "p_{T}^{e} [GeV]",  0, 500 if p.signal == "LQ3" else 200, [5,5,5] if p.signal == "LQ3" else [2,5,5]),
                var("Elec0Eta", "#eta^{e}", -3.5, 3.5, 5),
                ##var("Elec0Phi", "#phi^{e}", -R.TMath.Pi(), R.TMath.Pi(),2),                 
                label = "Presel."
                )
    
    p.addVarSet("ControlTau",    
                #var("Tau0Pt", "p_{T}^{#tau} [GeV]",  0, 500 if p.signal == "LQ3" else 200, [5,5,5], sysplots=["0tag2pjet_0ptv", "1tag2pjet_0ptv", "2tag2pjet_0ptv"]),
                var("Tau0Pt", "p_{T}^{#tau} [GeV]",  0, 500 if p.signal == "LQ3" else 400, [5,5,5], sysplots=["0tag2pjet_0ptv", "1tag2pjet_0ptv", "2tag2pjet_0ptv"]),
                var("Tau0Eta", "#eta^{#tau}", -3.5, 3.5, 5),
                ##var("Tau0Phi", "#phi^{#tau}", -R.TMath.Pi(), R.TMath.Pi(),2),
                var("Tau0Ntrk", "N_{trk}^{#tau}", 0, 5, 1, ssfact=1),
                var("Tau0BDTScore", "BDT^{#tau}", 0, 1, 2),
                label = "Presel."
                )
    
    p.addVarSet("ControlJet",   
                #var("JetPt", "p_{T}^{jets} [GeV]",  0, 200, [5,5,5]),
                #var("JetEta", "#eta^{jets}", -3.5, 3.5, 5),
                #var("JetPhi", "#phi^{jets}", -R.TMath.Pi(), R.TMath.Pi(),2),
                var("Jet0Pt", "p_{T}^{jet 0} [GeV]",  0, 500 if p.signal == "LQ3" else 200, [5,5,5]),
                var("Jet0Eta", "#eta^{jet 0}", -3.5, 3.5, 5),
                ##var("Jet0Phi", "#phi^{jet 0}", -R.TMath.Pi(), R.TMath.Pi(),2),
                var("Jet1Pt", "p_{T}^{jet 1} [GeV]",  0, 500 if p.signal == "LQ3" else 200, [5,5,5]),
                var("Jet1Eta", "#eta^{jet 1}", -3.5, 3.5, 5),
                ##var("Jet1Phi", "#phi^{jet 1}", -R.TMath.Pi(), R.TMath.Pi(),2),                 
                var("NSigDefJet", "NSigJet", -0.5, 10.5, 1, ssfact=1),
                label = "Presel."
                )
    
    p.addVarSet("ControlMET",
                var("MET", "E_{T}^{miss} [GeV]", 0, 400, 5),
                var("MTW", "M_{T}^{W} [GeV]", 0, 200, 4),
                #var("MTWEl", "M_{T}^{W} (e) [GeV]", 0, 200, 4),
                #var("MTWMu", "M_{T}^{W} (#mu) [GeV]", 0, 200, 4),
                var("METCentrality", "MET #phi centrality",1.5,1.5,1),
                var("DPhiLepMET", "#Delta#phi(l, MET)",0.0,3.2,2),
                var("DPhiTauMET", "#Delta#phi(#tau, MET)",0.0,3.2,2),
                #var("DPhiJetMET", "#Delta#phi(jet, MET)",0.0,3.2,2),
                label = "Presel. + m_{#tau#tau} cut"
                )

    p.addVarSet("ControlHighMTW",
                var("TauPt", "p_{T}^{#tau 0} [GeV]",  0, 200, 5),
                #sysplots=["0tag2pjet_0ptv", "1tag2pjet_0ptv", "2tag2pjet_0ptv"]),
                var("MET", "E_{T}^{miss} [GeV]",40,400, [5,10,10]),
                var("MTW", "M_{T}^{W} [GeV]", 40, 200, 4),                
                var("MTW_PreCut", "M_{T}^{W} [GeV]", 0, 200, 2),                
                #var("MTWEl", "M_{T}^{W} (e) [GeV]", 40, 200, 4),
                #var("MTWMu", "M_{T}^{W} (#mu) [GeV]", 40, 200, 4),
                var("METCentrality", "MET #phi centrality",1.5,1.5,2),
                var("DeltaPt", "#Deltap_{T}(lep,#tau)", -150, 150, 1),
                var("PtTauTau", "p_{T}(#tau,#tau)", 0, 500, 2),
                #    sysplots=["0tag2pjet_0ptv", "1tag2pjet_0ptv", "2tag2pjet_0ptv"]),
                var("Tau0BDTScore", "BDT^{#tau}", 0, 1, 2),
                #var("NJet", "NJet", -0.5, 10.5, 1, ssfact=1),                
                var("DPhiLepMET", "#Delta#phi(l, MET)",0.0,3.2,2),
                var("DPhiTauMET", "#Delta#phi(#tau, MET)",0.0,3.2,2),
                var("dRTauJetMin", "#DeltaR_{#taujet}", 0, 5, 1),
                var("dRLepTau", "#DeltaR_{l#tau}", 0, 5, 10),
                var("dPhiLepTau", "#Delta#phi_{l#tau}", 0, 5, 10),
                var("sT", "s_{T} [GeV]", 0, 1200, 5),
                #var("TauTruth", "PDG",  -50, 50, 1),
                label = "Presel. + high m_{T}", signal = True
                )

    p.addVarSet("ZeeCR",
                var("mLL", "m_{vis} [GeV]",0,200,[5,10,10]),
                var("mMMC", "m_{MMC}",0, 600, 20),
                var("TauPt", "p_{T}^{#tau 0} [GeV]",  0, 200, 5),
                #var("TauEta", "#eta^{#tau}", -3.5, 3.5, 5),
                #var("TauPhi", "#phi^{#tau}", -R.TMath.Pi(), R.TMath.Pi(),2),
                var("TauNtrk", "N_{trk}^{#tau}", 0, 5,1, ssfact=1),
                ##var("LepPt", "p_{T}^{e} [GeV]",  0, 200, [2,5,5]),
                #var("LepEta", "#eta^{e}", -3.5, 3.5, 5),
                label = "Z #rightarrow ee CR", signal = False
                )


    p.addVarSet("ZCR",
                #var("NJet", "NJet", -0.5, 10.5, 1, ssfact=1),
                var("TauPt", "p_{T}^{#tau 0} [GeV]",  0, 200, 5),                
                var("mLL", "m_{vis} [GeV]",0,200,[5,10,10]),
                var("mMMC", "m_{MMC}",0, 600, 20),
                #var("MET", "E_{T}^{miss} [GeV]", 0, 400, 5),
                ##var("MTW", "M_{T}^{W} [GeV]", 0, 200, 4),
                #var("PtTauTau", "p_{T}(#tau,#tau)", 0, 500, 2),
                #var("mjjCorr", "m_{jj} [GeV]", 0, 200, [5,10,10]),
                label = "Z #rightarrow #tau#tau CR", signal = False
                )
    
    if signal != "LQ3":
        p.addVarSet("Mtautau",
                    var("mLL", "m_{vis} [GeV]",0,200,[5,10,10],sysplots=["2tag2pjet_0ptv"]),
                    ##var("ElecmLL", "m_{vis} (e) [GeV]",0,200,10),
                    ##var("MuonmLL", "m_{vis} (#mu) [GeV]",0,200,10),
                    #var("mLL_MMCCut", "m_{vis} (MMC Cut) [GeV]",0, 150,[5,10,10]),                
                    var("mMMC", "m_{MMC}",0, 600, 20),
                    ##var("ElecmMMC", "m_{MMC} (e)",0, 600, 20),
                    ##var("MuonmMMC", "m_{MMC} (#mu)",0, 600, 20),
                    #var("mMMC_mLL", "m_{MMC} - mLL",-100, 200, 10),                
                    #var("TauPt", "p_{T}^{#tau 0} [GeV]",  0, 200, 5),                
                    label = "Antitop cuts"
                    )

        p.addVarSet("ControlMJJ",    
                    var("mjj", "m_{jj} [GeV]", 0, 200, [5,5,5]),
                    ##var("mjjUncorr", "m_{jj} (uncorr.) [GeV]", 0, 200, [5,5,5]),
                    label = "m_{#tau#tau} cut"
                    )
        
        p.addVarSet("HH",
                    var("mHH", "m_{HH} [GeV]", 200, 1200, 50, sysplots=["2tag2pjet_0ptv_SR"]),
                    var("TauPt", "p_{T}^{#tau} [GeV]",  0, 200, [5,5,5]),
                                #bins = (0, 257, 260, 270, 280, 290, 300, 310, 320, 400, 450, 2000), divide_width = True),
                    regions = ["SR", "SR_SS"],
                    #"lowmBBcr", "highmBBcr", "{lowmBBcr,highmBBcr}",
                    region_names = {"SR" : "", "SR_SS" : "SS"},
                    #"lowmBBcr" : "low m_{jj} SB", "highmBBcr" : "high m_{jj} SB", "{lowmBBcr,highmBBcr}" : "m_{jj} SB"},                    
                    dirname = "Res", label = "res. cuts"
                    )
        
    if signal == "LQ3":
        p.addVarSet("LQ",    
                    var("dRJetTauMax", "#DeltaR_{#taujet}", 0, 5, 10),
                    var("dRJetLepMax", "#DeltaR_{ljet}", 0, 5, 10),
                    var("dPhiJetTauMax", "#Delta#phi_{#taujet}", 0, 3.2, 10),
                    var("dPhiJetLepMax", "#Delta#phi_{ljet}", 0, 3.2, 10),
                    var("MtauJ_dR", "m_{#taujet} (dR) [GeV]", 0, 800, 4),
                    var("MlepJ_dR", "m_{lepjet} (dR) [GeV]", 0, 800, 4),                
                    var("MtauJ_dM", "m_{#taujet} (dM) [GeV]", 0, 800, 4),
                    var("MlepJ_dM", "m_{lepjet} (dM) [GeV]", 0, 800, 4),                
                    var("MtauJ_dPhi", "m_{#taujet} (dR) [GeV]", 0, 800, 4),
                    var("MlepJ_dPhi", "m_{lepjet} (dR) [GeV]", 0, 800, 4),                
                    var("PtTauJet", "p_{T}^{#taujet}", 0, 400, 5),
                    var("PtLepJet", "p_{T}^{lepjet}", 0, 400, 5),				    
                    var("MLQMin", "m_{LQ} (min) [GeV]", 0, 800, 4),
                    var("MLQMax", "m_{LQ} (max) [GeV]", 0, 800, 4),
                    var("MLQAve", "m_{LQ} (ave) [GeV]", 0, 800, 4),                                
                    var("sT", "s_{T} [GeV]", 0, 1200, 5),
                    var("dRTauJet", "#DeltaR_{#taujet}", 0, 5, 10),
                    var("dRLepJet", "#DeltaR_{#lepjet}", 0, 5, 10),                    
                    var("dEtaLQLQ", "#Delta#eta_{LQLQ}", 0, 5, 10),
                    var("dPhiLQLQ", "#Delta#phi_{LQLQ}", 0, 3.2, 10),                    
                    var("dRTauJetMin", "#DeltaR_{#taujet}", 0, 5, 1),
                    label = "Preselection"
                    )

        p.addVarSet("LQCuts",    
                    var("dRJetTauMax", "#DeltaR_{#taujet}", 0, 5, 10),
                    var("dRJetLepMax", "#DeltaR_{ljet}", 0, 5, 10),
                    var("dPhiJetTauMax", "#Delta#phi_{#taujet}", 0, 3.2, 10),
                    var("dPhiJetLepMax", "#Delta#phi_{ljet}", 0, 3.2, 10),
                    var("MtauJ_dR", "m_{#taujet} (dR) [GeV]", 0, 800, 4),
                    var("MlepJ_dR", "m_{lepjet} (dR) [GeV]", 0, 800, 4),                
                    var("MtauJ_dM", "m_{#taujet} (dM) [GeV]", 0, 800, 4),
                    var("MlepJ_dM", "m_{lepjet} (dM) [GeV]", 0, 800, 4),                
                    var("MtauJ_dPhi", "m_{#taujet} (dR) [GeV]", 0, 800, 4),
                    var("MlepJ_dPhi", "m_{lepjet} (dR) [GeV]", 0, 800, 4),                
                    var("PtTauJet", "p_{T}^{#taujet}", 0, 400, 5),
                    var("PtLepJet", "p_{T}^{lepjet}", 0, 400, 5),				    
                    var("MLQMin", "m_{LQ} (min) [GeV]", 0, 800, (7, 16, 25, 34, 43, 52, 61, 70, 79, 88, 98, 114, 178, 2000)),
                    var("MLQMax", "m_{LQ} (max) [GeV]", 0, 800, 4),
                    var("MLQAve", "m_{LQ} (ave) [GeV]", 0, 800, 4),                                
                    var("sT", "s_{T} [GeV]", 0, 1200, 5),
                    var("dRTauJet", "#DeltaR_{#taujet}", 0, 5, 10),
                    var("dRLepJet", "#DeltaR_{#lepjet}", 0, 5, 10),                    
                    var("dEtaLQLQ", "#Delta#eta_{LQLQ}", 0, 5, 10),
                    var("dPhiLQLQ", "#Delta#phi_{LQLQ}", 0, 3.2, 10),                    
                    label = "Cuts"
                    )

def BlindMethod():
        p.config.addWindowBlinding("MtauJ_dR","2tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MlepJ_dR","2tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MtauJ_dM","2tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MlepJ_dM","2tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MtauJ_dPhi","2tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MlepJ_dPhi","2tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MLQMin","2tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MLQMax","2tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MLQAve","2tag2pjet_0ptv","",0, 800)        
        #p.config.addWindowBlinding("sT","2tag2pjet_0ptv","",0, 1200)
        p.config.addWindowBlinding("MtauJ_dR","1tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MlepJ_dR","1tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MtauJ_dM","1tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MlepJ_dM","1tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MtauJ_dPhi","1tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MlepJ_dPhi","1tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MLQMin","1tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MLQMax","1tag2pjet_0ptv","",0, 800)
        p.config.addWindowBlinding("MLQAve","1tag2pjet_0ptv","",0, 800)        
        #p.config.addWindowBlinding("sT","1tag2pjet_0ptv","",0, 1200)


if __name__ == '__main__':      

    # Use steer2Tau.py to retrieve arguments
    p, options = load()
    
    p.addSystematics() 
    p.tags = [0,1,2]

    wrapAddVarSet(p.signal)
    
    BDTVars("BDTVarsPreselection", "presel.", p.signal)
    
    BDTVars("BDTVarsHighMTCR", "Presel. + top CR", p.signal, draw_signal = False)
    
    if p.signal == "LQ3":
        BDTVars("BDTVarsZCR", "Presel. + ZCR", p.signal, draw_signal = False)    
        BlindMethod()

    print ">>> plot2Tau::makePlots() is called."
    #print ">>>>  Processing variable sets : " + str(options.sets.split(","))
    print ">>>>  Processing tags          : None "
    p.makePlots(options.sets.split(',') if options.sets is not None else None,
                options.tags.split(',') if options.tags is not None else None,
                nproc = options.nproc
                )
    print ">>> plot2Tau::clearSet() is called."
    p.clearSet()

    if options.sets is None or "ControlJet" in options.sets.split(','):

        p.tags = ["0p"]
        p.addVarSet("ControlJet",   
                    var("NJet", "NJet", -0.5, 10.5, 1, ssfact=1),
                    var("NBJet", "NBJet", -0.5, 10.5, 1, ssfact=1),
                    label = "Presel."
                    )
        
        p.makePlots("ControlJet")        
        p.clearSet()

    if options.sets and "Run" in options.sets.split(','):        
        p.tags = ["0p", "2"]
        p.addVarSet("Run",
                    var("ZPV", "PV_{Z}", -100, 100, 1),                    
                    var("Yields", "Yields"),
                    )
        p.makePlots("Run")        
        p.clearSet()

    p.config.addBDTTransformation("trafoHH2", 14, 0.2, 0)
    p.config.addBDTTransformation("trafoHH4", 14, 0.4, 0)

    if p.signal == "LQ3":
        bdtcuts = ("Preselection","HighMTCR")
        for c in bdtcuts:
            if options.sets is None or "BDTScore"+c in options.sets.split(','):
                p.tags = [0,2]
                for m in [300, 500, 1000, 1500]:
                    p.addVarSet("BDTScore"+c,
                                var("LQ_BDT_%s" % m, "BDT Score m = %s GeV" % m, -1, 1, 20, var_type = R.Config.BDTOutput,
                                    sysplots=["0tag2pjet_0ptv", "1tag2pjet_0ptv", "2tag2pjet_0ptv"]),
                                label = c, sig_type = "LQ3", mass = str(m),
                                signal = False if c == "HighMTCR" else True
                                )
                    p.makePlots("BDTScore"+c)
                    p.clearSet()

                p.tags = [1]
                for m in [300, 500, 1000, 1500]:
                    p.addVarSet("BDTScore"+c,
                                var("LQ1tag_BDT_%s" % m, "BDT Score m = %s GeV" % m, -1, 1, 20, var_type = R.Config.BDTOutput,
                                    sysplots=["1tag2pjet_0ptv"]),
                                label = c, sig_type = "LQ3", mass = str(m),
                                signal = False if c == "HighMTCR" else True
                                )
                    p.makePlots("BDTScore1tag"+c)
                    p.clearSet()

    elif p.signal == "ZZ":
        bdtcuts = ("Preselection","HighMTCR")        
        for c in bdtcuts:
            if options.sets is None or "BDTScore"+c in options.sets.split(','):
                p.tags = [0,1,2]
                p.addVarSet("BDTScore"+c,                    
                            var("SM_BDT", "BDT Score m_{H} = None", -1, 1, 20, var_type = R.Config.BDTOutput,
                                sysplots=["0tag2pjet_0ptv", "1tag2pjet_0ptv", "2tag2pjet_0ptv"]),
                            label = c, sig_type = "ZZ", mass = None                   
                                    )        
                p.makePlots("BDTScore"+c) 
                p.clearSet()               

    else:   
        bdtcuts = ("Preselection","HighMTCR")
        for c in bdtcuts:
            if options.sets is None or "BDTScore"+c in options.sets.split(','):
                p.tags = [0,1,2]
                
                p.addVarSet("BDTScore"+c,
                            var("SM_BDT", "BDT Score m_{H} = None", -1, 1, 20, var_type = R.Config.BDTOutput,
                                sysplots=["0tag2pjet_0ptv", "1tag2pjet_0ptv", "2tag2pjet_0ptv"]),
                            #var("SMRW_BDT", "BDT Score RW m_{H} = None", -1, 1, 20, var_type = R.Config.BDTOutput,
                            #    sysplots=["0tag2pjet_0ptv", "1tag2pjet_0ptv", "2tag2pjet_0ptv"]),
                            label = c, sig_type = "H", mass = None                   
                            )        
                p.makePlots("BDTScore"+c) 


                if c == "Preselection":
                    p.addVarSet("BDTScore"+c,
                                #var("SM_BDT", "BDT Score m_{H} = None", -1, 1, 20, var_type = R.Config.BDTOutput,
                                #    sysplots=["0tag2pjet_0ptv", "1tag2pjet_0ptv", "2tag2pjet_0ptv"]),
                                var("SMRW_BDT", "BDT Score RW m_{H} = None", -1, 1, 20, var_type = R.Config.BDTOutput,
                                    sysplots=["0tag2pjet_0ptv", "1tag2pjet_0ptv", "2tag2pjet_0ptv"]),
                                label = c, sig_type = "HRW", mass = None, dirname = "BDTScorePreselectionRW"                   
                                )        
                    p.makePlots("BDTScore"+c) 


                p.clearSet()               
