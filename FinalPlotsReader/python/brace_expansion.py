def expand(text, sep='', suppress=False):
    "Do bash-like brace expansion"

    if not "{" in text: return [text]

    from MIASetup.pyparsing import (Suppress, Optional, CharsNotIn, Forward, Group,
                           ZeroOrMore, delimitedList)
    
    lbrack, rbrack = map(Suppress, "{}")
        
    optAnything = Optional(Group(CharsNotIn("{},")))
        
    braceExpr = Forward()
        
    listItem = optAnything ^ Group(braceExpr)

    bracketedList = Group(lbrack + delimitedList(listItem) + rbrack)

    if suppress:
        braceExpr << optAnything.suppress() + ZeroOrMore(bracketedList + optAnything.suppress())
    else:
        braceExpr << optAnything + ZeroOrMore(bracketedList + optAnything)
        
    result = braceExpr.parseString(text).asList()

    def product(first=None,*rest):
        if first is None: return
        
        if not rest:
            for elephant in first: yield elephant
        else:
            for elephant in first:
                for item in product(*rest):
                    yield elephant + sep + item

    def lst_concatenate(lst):
        if isinstance(lst,str):
            return [lst]
        res = []
        for item in map(lst_extend,lst):
            res.extend(item)
        return res

    def lst_extend(lst):
        return list(product(*map(lst_concatenate,lst)))

    expanded = list(lst_extend(result))
    
    if suppress and not expanded:
        expanded = ['']
        
    return expanded
