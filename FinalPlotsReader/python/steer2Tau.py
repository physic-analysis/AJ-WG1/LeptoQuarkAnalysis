import ROOT as R
from plotting import PlotterDict, uniqify
from plotting2Tau import Plotter2Tau as Plotter

def load(name = None):

    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-s", "--sets"   , dest="sets"   , help="run over SETS variable sets", metavar="SETS", default = None)
    parser.add_option("-m", "--mass"   , dest="mass"   , help="Use signal mass MASS for control and resontant plots", metavar="MASS", default = "300")
    parser.add_option(""  , "--type"   , dest="type"   , help="Use signal type TYPE for control and resontant plots", metavar="TYPE", default = "G")    
    parser.add_option("-l", "--level"  , dest="level"  , help="run all plots with level <= LEVEL", metavar="LEVEL", type="int", default = 0)    
    parser.add_option("-t", "--tags"   , dest="tags"   , help="run over TAG tags", metavar="TAG", default = None)
    parser.add_option(""  , "--tt"     , dest="tt"     , help="run with truth tagging (1) or not (0)", default = 1)
    parser.add_option("-n", "--nproc"  , dest="nproc"  , help="run NUM variable sets in parallel", metavar="NUM", type="int", default = 1)
    parser.add_option("-v", "--version", dest="version", help="run plotting using VER plotter", metavar="VER", default = None)        
    parser.add_option(""  , "--nosysts", dest="nosysts", help="Turn off systematics", default = False, action="store_true")        
    parser.add_option(""  , "--inname" , dest="inname" , help="Specify the input file name", metavar="INNAME", default = "merged")    
    parser.add_option(""  , "--outname", dest="outname", help="Specify the output file name", metavar="OUTNAME", default = "plots_lq")    
    (options, args) = parser.parse_args()

    pd = PlotterDict()
    
    pd += Plotter("/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/MIA/run/BlueTuesday/FullDataDrivenFakes/",
                  inname  = options.inname,
                  outname = options.outname,
                  title = "LQLQ#rightarrowbb#tau_{lep}#tau_{had}", 
                  signal = options.type,
                  mass = options.mass, 
                  lumi = "43.59", 
                  tt = options.tt, 
                  level = options.level,
                  data_fakes = True, 
                  tags = options.tags, 
                  systs = not options.nosysts)

    if name is None: name = options.version
    p = pd[name] if name is not None else pd.last
    print ">>> Loading plotter '", p.outname, "' reading from", uniqify(p.paths)
    p.sig_colours = {"300" : R.kRed+3, "500" : R.kRed-6, "1000" : R.kRed-10,
                     "200" : R.kRed+3, "700" : R.kRed-6, "800" : R.kRed-6,
                     "1500" : R.kRed-10}
    
    return p, options

if __name__ == "__main__":
    load()
