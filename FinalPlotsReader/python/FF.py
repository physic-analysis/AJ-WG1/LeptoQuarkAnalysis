import sys
import ROOT as R
from plotting2Tau import Plotter2Tau as Plotter
from steer2Tau import load
from MIA.brace_expansion import expand

def makepos(h, hn, hd):
    "make sure all bins in `h` are positive"
    for i in xrange(1, h.GetNbinsX()+1):
        # Both num and den < 0 (i.e. MC overestimates data in both) then we would get a +ve ff although it
        # really means that we want to redice the background (i.e. a -ve FF).  For now set ff to 0.
        if h.GetBinContent(i) < 0 or (hn.GetBinContent(i) < 0 and hd.GetBinContent(i) < 0):
            h.SetBinContent(i, 0)

        # Prevent mad values with large error: if FF > 1 and error consitent with 0 set to 0
        if h.GetBinContent(i) > 1 and (h.GetBinContent(i) - h.GetBinError(i)) <= 0:
             h.SetBinContent(i, 0)
            
    return h

def write(h, out, name):
    "Write `h` to dir `out` with name `name` in file `fout`"
    global fout
    if not h or not fout: return

    if out.startswith('/'):
        out = out[1:]
    
    fout.cd()
    for d in out.split('/'):
        if not d: continue
        if not R.gDirectory.Get(d):
            R.gDirectory.mkdir(d)
        R.gDirectory.cd(d)
 
    h.Write(name)
    return 

def bins(var):
    "Specify FF binning for various vars, `var`"
    if var == "TauPt" or var == "TauPt50" or var == "TauPt40":
        if lq3:
            binning = (25, 30, 35, 40, 50, 60, 100, 200)
        else:
            binning = (20, 25, 30, 35, 40, 50, 60, 70, 120, 200) #(20, 30, 40, 70, 200)
    elif var == "LepPt":
        binning = (20, 25, 30, 35, 40, 50, 60, 70, 120, 200)
    elif var == "MTW":
        binning = (0, 20, 40, 60, 80, 120, 160, 200)
    elif var == "Tau0Eta":
        binning = 7
    elif var == "mLL":
        binning = 10
    elif "DPhi" in var:
        binning = 4
    elif "sT" in var:
        binning = (0, 20, 40, 60, 80, 100, 150, 200, 250, 300, 350)
    else:
        binning = 1            

    return binning

def sanitise(region):
    "Sanitise name of region for using as output"
    if '{' in region:
        return  region.replace(" ", "").replace("{", "").replace("}", "").replace(",_", "+").replace(",", "+")
    else:
        return region

def contamination(hists, region, np, ntag):
    # fake-tau W+jets
    wjets = None    
    for s in wsamples:
        if not s in hists or not hists[s]:
            continue
        try:
            wjets.Add(hists[s])
        except AttributeError:
            wjets = hists[s].Clone()

    print "CONTAMINATION ({}, {}p, {}b) : {:.2f} {:.2f} {:.2f} {:.2f} ".format(region, np, ntag, wjets.Integral(), hists["ttbarFake"].Integral(), wjets.Integral()/(hists["ttbarFake"].Integral()+wjets.Integral())*100, hists["ttbarFake"].Integral()/ (hists["ttbarFake"].Integral()+wjets.Integral())*100)


def plotff(pnum, pden, cut, lep, np, ntag, var="TauPt", region="CR", veto = [], syst = ""):
    "Caluclate fake factors" 
    print "Calculate fake factors : "
    global cuts
    binning = bins(var)

    if lep:
        name  = "FF_All/"+cut+"/Np"+np+"/"+lep
    else:
        name  = "FF_All/"+cut+"/Np"+np

    # Setup systematics
    scale = False
    if syst:
        if not "SysSubtraction_" in syst and not "SysSS" in syst and not "SysTTbarAcc" in syst:
            ovar = var
            name += "/Systematics" 
            var  += "_" + syst
            oname = name + "_" + syst
            oregion = region
        else:
            ovar = var
            oregion = region

            if "SysSubtraction_" in syst:
                tok = syst.split("__")
                scale = +1 if tok[1] == "1up"  else -1
                #scale[tok[1]] = float(tok[2])

            if "SysSS" in syst:
                region += "_SS"
                    
            # For consistent naming of output
            oname = name + "/Systematics" + "_" + syst

    else:
        # For consistent naming of output
        oname = name
        ovar = var
        oregion = region

    print "oname["+ oname + "], ovar["+ str(ovar) + "], oregion[" + oregion + "]"
        
    c = R.TCanvas()


    # SF for Sherpa
    scaleSh = {}
    scaleSh["Zbb"] = scaleSh["Zcc"] = scaleSh["Zbc"] = 1.4
    scaleSh["Zttbb"] = scaleSh["Zttcc"] = scaleSh["Zttbc"] = 1.4

    # Plot numerator (with MC fakes)
    try:
        print "Plot numerator (with MC fakes) : " + name + " " + ntag + "tag2pjet_0ptv_"+region
        data, hs, _ = pnum.readHist(name, expand(ntag+"tag2pjet_0ptv_"+region), var, rebin = binning, stack = True, scale=scaleSh, scale_bkg_ff=scale)
        print str(data) +" " + str(hs) + " " + str(_)
        data.Draw("ep")        
        hs.Draw("histsame")
        data.Draw("epsame")
        leg = pnum.makeLegend()
        leg.Draw()
        print "c.Print: Fakes/" + oname.replace("/", "_").replace("FF", sanitise(oregion).replace("CR", "SR"))+sanitise(ntag)+"tag."+ovar+".png"
        c.Print("Fakes/" + oname.replace("/", "_").replace("FF", sanitise(oregion).replace("CR", "SR"))+sanitise(ntag)+"tag."+ovar+".png")
        R.gPad.SetLogy(True)
        c.Print("Fakes/" + oname.replace("/", "_").replace("FF", sanitise(oregion).replace("CR", "SR"))+sanitise(ntag)+"tag."+ovar+".log.png")
        R.gPad.SetLogy(False)
    except KeyError:
        print "Skipping (num) ", name, expand(ntag+"tag2pjet_0ptv_"+region), var
        return     

    # Get data - real tau bkg for numerator
    #print ">NUM"
    print "hnum = punm.readHist(...)"
    hnum = pnum.readHist(name, expand(ntag+"tag2pjet_0ptv_"+region), var, rebin = binning, bkg_veto = veto, scale=scaleSh, scale_bkg_ff=scale) 
    hn = hnum['data']
    bn = hnum['bkg']
    hn.Add(bn, -1)
    #print "<NUM"

#     if not syst: # and not "SS" in region:
#         contamination(hnum, "ID SS" if "SS" in region else "ID", np, ntag)
    

    # Plot denomenator (with MC fakes)
    try:
        print "PLot denomenator (with MC fakes) : " + name + " " + ntag+"tag2pjet_0ptv_"+region
        data, hs, _ = pden.readHist(name, expand(ntag+"tag2pjet_0ptv_"+region), var, rebin = binning, stack = True, scale=scaleSh, scale_bkg_ff=scale)
        data.Draw("ep")
        hs.Draw("histsame")
        data.Draw("epsame")
        leg = pden.makeLegend()
        leg.Draw()
        print "c.Print : Fakes/" + oname.replace("/", "_").replace("FF", sanitise(oregion))+sanitise(ntag)+"tag."+ovar+".png"
        c.Print("Fakes/" + oname.replace("/", "_").replace("FF", sanitise(oregion))+sanitise(ntag)+"tag."+ovar+".png")
        R.gPad.SetLogy(True)
        c.Print("Fakes/" + oname.replace("/", "_").replace("FF", sanitise(oregion))+sanitise(ntag)+"tag."+ovar+".log.png")        
        R.gPad.SetLogy(False)
    except KeyError:
        print "Skipping (den)", name, expand(ntag+"tag2pjet_0ptv_"+region), var
        return

    # Get data - real tau bkg for denomenator
    #print ">DEN"
    print "hden = pden.readHist(...)"
    hden = pden.readHist(name, expand(ntag+"tag2pjet_0ptv_"+region), var, rebin = binning, bkg_veto = veto, scale=scaleSh, scale_bkg_ff=scale)
    hd = hden['data']
    bd = hden['bkg']
    hd.Add(bd, -1)
    print "Get data - real tau bkg for numberator "
    print "hnum" + str(hnum)
    print "hn" + str(hn)
    print "bn" + str(bn) 
    print "Get data - real tau bkg for denomenator"
    print "hden" + str(hden)
    print "hd" + str(hd)
    print "bd" + str(bd) 
    #print "<DEN"

#     if not syst: # and not "SS" in region:
#         contamination(hden, "ANTI-ID SS" if "SS" in region else "ANIT-ID", np, ntag)

    # Plot/write
    r = hn.Clone()  
    r.Divide(hd)
    makepos(r, hn, hd)
    print "SetTitle : FakeFactors for " + sanitise(region) 
#    r.SetTitle("FakeFactors for " + sanitise(region) + " ; " + var + "; FF")
    r.SetTitle("FakeFactors : " + sanitise(cut) + "/" + sanitise(np) + "Prong/" + ntag + " b-tag ; " + var + "; FF")

    # r.SetMinimum(-0.5)
    # r.SetMaximum(5)
    r.Draw("e")

    l = R.TLine(20, 1, 200, 1)
    l.SetLineColor(2)
    l.SetLineStyle(2)
    l.Draw()

    write(r, "/".join([syst, sanitise(oregion), "All", cut, lep, "Np" + np, "Nb"+sanitise(ntag)]), ovar)
    outname = "Fakes/" +  oname.replace("/", "_")+"."+sanitise(oregion)+"."+sanitise(ntag)+"tag."+ovar+".png"
    print outname
    print "plotff FINISH"
    print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    c.Print(outname)

    return r, outname

def plotrQCD(pcr, cut, lep, np, ntag, var="LepPt", region = "CR", syst = "", doRw = True, doRtop = True):
    "Calculate MC-reliant rQCD"
    
    global wsamples
    binning = bins(var)
    
    name  = "FF_All/"+cut+"/Np"+np+"/"+lep

    # Setup systematics
    scale = False
    if syst:
        if not "SysSubtraction_" in syst and not "SysSS" in syst and not "SysTTbarAcc" in syst:
            ovar = var
            name += "/Systematics" 
            var  += "_" + syst
            oname = name + "_" + syst
            oregion = region
        else:
            ovar = var
            oregion = region

            if "SysSubtraction_" in syst:
                tok = syst.split("__")
                scale = +1 if tok[1] == "1up"  else -1
                #scale[tok[1]] = float(tok[2])

            if "SysSS" in syst:
                region += "_SS"

#             if "SysTTbarAcc" in syst:
#                 # Must come after all others or need to switch back to nominal
#                 tok = syst.split("_")
#                 altsample = tok[1]
# 
#                 for ntt in ("ttbar", "ttbarlephad", "ttbarradHi", "ttbarradLo", "ttbarHwg", "ttbaraMC"):
#                     pcr.config.removeSample(ntt)
#                     pcr.config.removeSample(ntt + "Fake")                    
#                     
#                 pcr.config.addBackgroundSample(altsample, altsample, R.kOrange)
# 
#                 if not "ttbarFake" in veto:
#                     pcr.config.addBackgroundSample(altsample+"Fake", altsample+"Fake", R.kOrange-6)                    
#             else:
#                 # Reset to nominal ttbar
#                 for ntt in ("ttbar", "ttbarlephad", "ttbarradHi", "ttbarradLo", "ttbarHwg", "ttbaraMC"):
#                     pcr.config.removeSample(ntt)
#                     pcr.config.removeSample(ntt + "Fake")
#                     
#                 pcr.config.addBackgroundSample("ttbar", "ttbar", R.kOrange)
#                 
#                 if not "ttbarFake" in veto:
#                     pcr.config.addBackgroundSample("ttbarFake", "ttbarFake", R.kOrange-6)
                    
            # For consistent naming of output
            oname = name + "/Systematics" + "_" + syst

    else:
        # For consistent naming of output
        oname = name
        ovar = var
        oregion = region

#         # Reset to nominal ttbar
#         for ntt in ("ttbar", "ttbarlephad", "ttbarradHi", "ttbarradLo", "ttbarHwg", "ttbaraMC"):
#             pcr.config.removeSample(ntt)
#             pcr.config.removeSample(ntt + "Fake")
#         
#             pcr.config.addBackgroundSample("ttbar", "ttbar", R.kOrange)
#             
#             if not "ttbarFake" in veto:
#                 pcr.config.addBackgroundSample("ttbarFake", "ttbarFake", R.kOrange-6)

    # SF for Sherpa
    scaleSh = {}
    scaleSh["Zbb"] = scaleSh["Zcc"] = scaleSh["Zbc"] = 1.4 
    scaleSh["Zttbb"] = scaleSh["Zttcc"] = scaleSh["Zttbc"] = 1.4

    # Get hists
    print "readHist(name=" + name
    try:     
        hists = pcr.readHist(name, expand(ntag+"tag2pjet_0ptv_"+region), var, bkg_veto = wsamples + ["ttbarFake"], scale = scaleSh, scale_bkg_ff=scale, rebin = binning)
    except KeyError:
        return
    
    c = R.TCanvas()

    # all fakes = data - true tau MC
    fake = hists['data'].Clone()        

    fake.Add(hists['bkg'], -1)

    # fake-tau W+jets
    wjets = None    
    print "wsamples : " + str(wsamples)
    print "hists :" + str(hists)
    for s in wsamples:
        #print "s=" + s + ", hists=" + str(hists) #+ ", hists[s]=" + hists[s]
        #print hists['data']
        if not s in hists or not hists[s]:
            continue
        try:
            print "wjets.Add(hists[s])--- : " + s
            wjets.Add(hists[s])
        except AttributeError:
            wjets = hists[s].Clone()
    #print wjets
    # qcd fakes = all fakes - w+jets fakes - ttbar fakes
    qcd = fake.Clone()
    qcd.Add(wjets, -1)
    try: 
        qcd.Add(hists['ttbarFake'], -1)
    except KeyError:
        pass

    # Plot/write
    qcd.SetTitle("rQCD ; " + var + "; rQCD")

    r = qcd.Clone()
    r.Divide(fake)
    makepos(r,r,r)
    r.Draw("e")

    l = R.TLine(20, 1, 200, 1)
    l.SetLineColor(2)
    l.SetLineStyle(2)
    l.Draw()

    write(r, "/".join([syst, "rQCD_" + sanitise(region), "All", cut, lep, "Np" + np, "Nb"+sanitise(ntag)]), ovar)
    c.Print("Fakes/"+oname.replace("/", "_").replace("FF_All", "rQCD_All")+"."+sanitise(oregion)+"."+sanitise(ntag)+"tag."+ovar+".png")
    c.Clear()
    
    # Also calc rW ...
    if not doRw:
        return

#     W = fake.Clone()
#     W.Add(qcd, -1)
#     try: 
#         qcd.Add(hists['ttbarFake'], -1)
#     except KeyError:
#         pass
 
    print "wjets.Clone()"
    if not wjets : return
    rW = wjets.Clone()
    rW.SetTitle("rW ; " + var + "; rW")
    rW.Divide(fake)
    makepos(rW,rW,rW)
    rW.Draw("e")

    l = R.TLine(20, 1, 200, 1)
    l.SetLineColor(2)
    l.SetLineStyle(2)
    l.Draw()

    write(rW, "/".join([syst, "rW_" + sanitise(region), "All", cut, lep, "Np" + np, "Nb"+sanitise(ntag)]), ovar)
    c.Print("Fakes/"+oname.replace("/", "_").replace("FF_All", "rW_All")+"."+sanitise(oregion)+"."+sanitise(ntag)+"tag."+ovar+".png")


    # Also calc rTop ...
    if not doRtop:
        return

#     top = fake.Clone()
#     top.Add(qcd, -1)
#     top.Add(wjets, -1)

    try: 
        print "rTop Clone()"
        rTop = hists['ttbarFake'].Clone()
    except KeyError:
        return
    
    rTop.Divide(fake)
    rTop.SetTitle("rTop ; " + var + "; rTop")
    makepos(rTop,rTop,rTop)
    rTop.Draw("e")

    l = R.TLine(20, 1, 200, 1)
    l.SetLineColor(2)
    l.SetLineStyle(2)
    l.Draw()

    write(rTop, "/".join([syst, "rTop_" + sanitise(region), "All", cut, lep, "Np" + np, "Nb"+sanitise(ntag)]), ovar)
    c.Print("Fakes/"+oname.replace("/", "_").replace("FF_All", "rTop_All")+"."+sanitise(oregion)+"."+sanitise(ntag)+"tag."+ovar+".png")


    return

def plotMCFakeRatio(pcr, cut, lep, np, ntag, var="LepPt", region = "CR"):
    "OBSOLETE"
    global wsamples
    binning = bins(var)
    
    name  = "FF_All/"+cut+"/Np"+np+"/"+lep

    try:     
        hists = p.readHist(name, ntag+"tag2pjet_0ptv_"+region, var, bkg_veto = wsamples + ["ttbarFake"], rebin = binning)
    except KeyError:
        return
    
    c = R.TCanvas()

    wjets = None    
    for s in wsamples:
        try:
            if not hists[s]: continue
        except KeyError:
            continue
            
        try:
            wjets.Add(hists[s])
        except AttributeError:
            wjets = hists[s].Clone()
            
    ttbar = hists['ttbarFake']
    ttbar.SetTitle("Fake rate ; " + var + "; W/ttbar")

    r = wjets.Clone()
    r.Divide(ttbar)
    r.Draw("e")

    l = R.TLine(20, 1, 200, 1)
    l.SetLineColor(2)
    l.SetLineStyle(2)
    l.Draw()

    c.Print("Fakes/"+name.replace("/", "_").replace("FF_All", "MCRatio_All")+"."+region+"."+ntag+"tag."+var+".png")
    return

def ratioSSOS(pnum, pden, cut, lep, np, ntag, var="TauPt", region="CR", veto = []):

    global cuts
    binning = bins(var)

    if lep:
        name  = "FF_All/"+cut+"/Np"+np+"/"+lep
    else:
        name  = "FF_All/"+cut+"/Np"+np

    # SF for Sherpa
    scaleSh = {}
    scaleSh["Zbb"] = scaleSh["Zcc"] = scaleSh["Zbc"] = 1.4 
    scaleSh["Zttbb"] = scaleSh["Zttcc"] = scaleSh["Zttbc"] = 1.4

    # AntiID
    hdenos = pden.readHist(name, expand(ntag+"tag2pjet_0ptv_"+region), var, rebin = binning, bkg_veto = veto, scale=scaleSh) #, scale_bkg_ff=scale)
    hdenss = pden.readHist(name, expand(ntag+"tag2pjet_0ptv_"+region+"_SS"), var, rebin = binning, bkg_veto = veto, scale=scaleSh) #, scale_bkg_ff=scale)    

    # ID
    hnumos = pnum.readHist(name, expand(ntag+"tag2pjet_0ptv_"+region), var, rebin = binning, bkg_veto = veto, scale=scaleSh) #, scale_bkg_ff=scale)
    hnumss = pnum.readHist(name, expand(ntag+"tag2pjet_0ptv_"+region+"_SS"), var, rebin = binning, bkg_veto = veto, scale=scaleSh) #, scale_bkg_ff=scale)    

    for s in hnumos.iterkeys():
        if s in hdenos and s in hdenss and hdenos[s] and hdenss[s]:
            r = hdenss[s].Clone()
            r.Divide(hdenos[s])
            c = R.TCanvas()
            r.Draw("e")
            c.Print("Fakes/SSOS/"+name.replace("/", "_").replace("FF_All", "CR_SSOS_"+s)+"."+region+"."+ntag+"tag."+var+".png")
            
        if s in hnumos and s in hnumss and hnumss[s] and hnumss[s]:
            r = hnumss[s].Clone()
            r.Divide(hnumos[s])
            c = R.TCanvas()
            r.Draw("e")
            c.Print("Fakes/SSOS/"+name.replace("/", "_").replace("FF_All", "SR_SSOS_"+s)+"."+region+"."+ntag+"tag."+var+".png")


        if s in hdenos and s in hdenss and hdenos[s] and hdenss[s] and s in hnumos and s in hnumss and hnumss[s] and hnumss[s]:
            ranti = hdenss[s].Clone()
            ranti.Divide(hdenos[s])
            rid = hnumss[s].Clone()
            rid.Divide(hnumos[s])            
            c = R.TCanvas()
            rid.Divide(ranti)
            rid.Draw("e")
            c.Print("Fakes/SSOS/"+name.replace("/", "_").replace("FF_All", "Ratio_SSOS_"+s)+"."+region+"."+ntag+"tag."+var+".png")

    return

def comp(res, outname = [], esimple=True):
    "Compare array of fakefactors in `res`.  Nominal is first hist and remainder are systs"

    if not res: return
    hnom = res[0]
    hsys = hnom.Clone()

    for isys,h in enumerate(res[1:]):
        for i in xrange(1, hnom.GetNbinsX()+1):
            nnom = hnom.GetBinContent(i)
            n = h.GetBinContent(i)
            enom = hnom.GetBinError(i)
            e = h.GetBinError(i)                        

            if n and nnom:
                ratio = n/nnom 
                sys = ratio - 1

                if esimple:
                    # Error only on syst as is a subset of nom with less stats
                    esys = e/nnom
                else:
                    # Fully correlated error
                    rho = 1
                    esys2 = (enom * ratio / n)**2 + (e * ratio/nnom)**2 - 2 * ratio * rho * enom * e / nnom**2
                    try:
                        esys = esys2**0.5        
                    except ValueError:
                        print "WARNING: negative value for syst.  Setting to 0"
                        esys = 0

                hsys.SetBinContent(i, sys)
                hsys.SetBinError(i, esys)                        
            else:
                hsys.SetBinContent(i, 0)
                hsys.SetBinError(i, 0)                        
        
        c = R.TCanvas()
        hsys.SetTitle(outname[isys+1].replace("FF_All_", "").replace("_Systematics", "").replace(".png", "") + ";TauPt;Syst")
        hsys.SetMinimum(-1)
        hsys.SetMaximum(1)
        hsys.Draw("e")
        c.Print(outname[isys+1].replace("FF_All", "Syst_All"))

    return

if __name__ == '__main__':
    R.gROOT.SetBatch(True)

    fout = None
    wsamples = ['WlFake', 'WclFake', 'WccFake', 'WblFake', 'WbcFake', 'WbbFake'] + ['WttlFake', 'WttclFake', 'WttccFake', 'WttblFake', 'WttbcFake', 'WttbbFake']
    wsamples += ['WMGlFake', 'WMGclFake', 'WMGccFake', 'WMGblFake', 'WMGbcFake', 'WMGbbFake'] + ['WttMGlFake', 'WttMGclFake', 'WttMGccFake', 'WttMGblFake', 'WttMGbcFake', 'WttMGbbFake']    
    wsamples += ['WMGFake', 'WttMGFake']
    wsamples += ['WFake', 'WttFake']    
    cuts = ("Preselection", "HighMTW") #, "Mtautau", "MTW", "TopVeto", "Mbb", "Mtautau_NoMTW")
    veto = ["ttbarFake"] + wsamples
    regions = ("",) #("_2015", "_2016", "{_2015,_2016}")
    #systematics = ["", "SysANTITAU_BDT_CUT", "SysSubtraction_bkg__1down", "SysSubtraction_bkg__1up"]
    systematics = [""]

    zmg = False
    wmg = False
    lq3 = True

    if lq3:
        cuts = ("Preselection", "HighMTW") #, "ZCR") 
        dnuminv = "plots_lq_gc_ff_inviso_sr_2"
        ddeninv = "plots_lq_gc_ff_inviso_cr_2"
        dnumiso = "plots_lq_gc_ff_iso_sr_2" 
        ddeniso = "plots_lq_gc_ff_iso_cr_2"   
    else:
        dnuminv = "plots_ff_inviso_sr_newcdi"
        ddeninv = "plots_ff_inviso_cr_newcdi"
        dnumiso = "plots_ff_iso_sr_newcdi"
        ddeniso = "plots_ff_iso_cr_newcdi"

    # QCD invisol
    pnuminv, options = load(dnuminv)
    pnuminv.addSamples(sig = False, split_w_fake = True, zmg = zmg, wmg = wmg, shade_fakes = False)
    pdeninv, options = load(ddeninv)
    pdeninv.addSamples(sig = False, split_w_fake = True,zmg = zmg, wmg = wmg, shade_fakes = False)
  
    # wjets/ttbar + QCD SS
    pnumiso, options = load(dnumiso)
    pnumiso.addSamples(sig = False, split_w_fake = True, zmg = zmg, wmg = wmg, shade_fakes = True)
    pdeniso, options = load(ddeniso)
    pdeniso.addSamples(sig = False, split_w_fake = True, zmg = zmg, wmg = wmg, shade_fakes = True)
    
    fout = R.TFile.Open("Fakes/FF.data.root", "RECREATE")

   # QCD
    print "#############################################"
    print "                                             "
    print "           Fake factor for QCD               "
    print "                                             "
    print "#############################################"
    for r in regions:
        for c in cuts:
            for var in (("TauPt",) if lq3 else ("TauPt",)): #, "Tau0Eta", "MTW", "NJet", "NBJet", "DPhiLepMET", "DPhiTauMET", "LepPt"):
                for l in ("", "Elec", "Muon"):
                    for nProng in ("1", "3"):
                        for nBjets in ("0", "1", "2"):
                            res, ssres, name, namess = [], [], [], []
                            for s in systematics:
                                try:
                                    ff,n = plotff(pnuminv, pdeninv, c, l, nProng, nBjets, var, region="CR"+r, syst=s) #, veto=veto)
                                    res.append(ff)
                                    name.append(n)
                                    ffss,nss = plotff(pnuminv, pdeninv, c, l, nProng, nBjets, var, region="CR"+r+"_SS", syst=s) #, veto=veto)                    
                                    ssres.append(ffss)
                                    namess.append(nss)
                                except TypeError:
                                    pass
                                
                            comp(res, name)
                            comp(ssres, namess)                            

                            # SS
                            plotff(pnumiso, pdeniso, c, l, nProng, nBjets, var, region="CR"+r, syst="SysSS")

    # ttbar/wjets
    print "#############################################"
    print "                                             "
    print "        Fake factor for ttbar/Wjets          "
    print "                                             "
    print "#############################################"
    for r in regions:
        for c in cuts:
            for var in (("TauPt", "sT", "DPhiLepMET") if lq3 else ("TauPt40",)):# "TauPt", "TauPt50", "TauPt40"): #, "Tau0Eta", "MTW", "NJet", "NBJet", "DPhiLepMET", "DPhiTauMET", "LepPt"):                
                for l in ("", "Elec", "Muon"):
                    for np in ("1", "3"):
                        for nb in ("0", "1", "2"):
                            res, ssres, name, namess = [], [], [], []
                            for s in systematics:
                                try:
                                    ff, n = plotff(pnumiso, pdeniso, c, l, np, nb, var, region="HighMtCR"+r, veto = veto, syst=s)
                                    res.append(ff)
                                    name.append(n)
                                    ffss,nss = plotff(pnumiso, pdeniso, c, l, np, nb, var, region="HighMtCR"+r+"_SS", veto = veto, syst=s)
                                    ssres.append(ffss)
                                    namess.append(nss)
                                except TypeError:
                                    pass  

                                comp(res, name)
                                comp(ssres, namess)
#                                
    pcr, options = load(ddeniso)
    pcr.addSamples(sig = False, split_w_fake = True, zmg = zmg, wmg = wmg, shade_fakes = False)

    # rQCD
    print "#############################################"
    print "                                             "
    print "        rQCD calculation                     "
    print "                                             "
    print "#############################################"
    for r in regions:
        for c in cuts:
            for var in (("TauPt", "sT") if lq3 else ("TauPt",)): #, "Tau0Eta", "MTW", "NJet", "NBJet", "DPhiLepMET", "DPhiTauMET", "LepPt"):
                for l in ("Elec", "Muon"):
                    for np in ("1", "3"):
                        for nb in ("0"): 
                            for s in systematics:
                                plotrQCD(pcr, c, l, np, nb, var, region="CR"+r, syst=s)
                                plotrQCD(pcr, c, l, np, nb, var, region="CR"+r+"_SS", syst=s)
                        for nb in ("1"): 
                            for s in systematics:
                                plotrQCD(pcr, c, l, np, nb, var, region="CR"+r, syst=s, doRw=False)
                                plotrQCD(pcr, c, l, np, nb, var, region="CR"+r+"_SS", syst=s, doRw=False)
                        for nb in ("2",):
                            for s in ("","SysANTITAU_BDT_CUT"):
                                plotrQCD(pcr, c, l, np, nb, var, region="CR"+r, syst=s, doRw=False)
                                plotrQCD(pcr, c, l, np, nb, var, region="CR"+r+"_SS", syst=s, doRw=False)
                
    fout.Close()
