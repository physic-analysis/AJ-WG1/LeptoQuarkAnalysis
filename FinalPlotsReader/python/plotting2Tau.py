import os
import ROOT as R
from plotting import Plotter

class Plotter2Tau(Plotter):

    def __init__(self, *args, **kwargs):
        if not "regions" in kwargs: kwargs["regions"] = ["", "SS"]
        if not "tags" in kwargs: kwargs["tags"] = [0,1,2]
        if not "signal" in kwargs: kwargs["signal"] = "G"
        if not "mass" in kwargs: kwargs["mass"] = "300,500,1000"        

        super(Plotter2Tau, self).__init__(*args, **kwargs)

        self.config.scaleSignalToBackground(True)
        self.config.readScaleFactors(os.path.expandvars("./python/SF_2tau.txt"))

        pass

    def addSamples(self,
            tag = "2",
            sig = True, 
            mass = '900', 
            sig_type = None,
            split_w_fake = True,
            data_fakes = False,
            zmg = False,
            wmg = False,
            comb_w_flav = True, 
            bkg = True, 
            data = True,
            shade_fakes = False): 
        "Add the necessary samples"
        
        print ">>> plotting2Tau::addSamples() Enter:"

        sig_type = self.signal if sig_type is None else sig_type
        sig_type = [sig_type] if isinstance(sig_type, basestring) else sig_type

        print ">>> Check the sig type:"
        for check_sig_type in sig_type:
            print "addSamples - sig_type = " + check_sig_type

        if data:
            self.config.addDataSample("data", "Data", 1)

        print ">>> Try to add signal samples:"
        if sig:
            mass = mass.split(',') if isinstance(mass, basestring) else [mass]
            for mass_point in reversed(mass):
                if mass_point is None or mass_point == "None":
                    if len(sig_type) == 1 and sig_type[0] == "ZZ":
                        self.config.addSignalSample("ZZllqq", "ZZllqq", self.sig_colours.get(mass_point, R.kRed))                                                        
                        self.config.addBackgroundSample("hhttbb",  "hh (non-res)", R.kMagenta+4)
                    else:
                        if len(sig_type) != 1 or "RW" in sig_type[0]: self.config.addSignalSample("hhttbbRW",  "hh (non-res, RW)", R.kRed+3)                        
                        else: self.config.addSignalSample("hhttbb",  "hh (non-res)", R.kRed)

                else:
                    for s in sig_type:
                        if s == "LQ3":
                            print "LQ3 signal samples will be added.."
                            #self.config.addSignalSample(s + "btaubtau%s" % m,  s + "(%s)" % m, self.sig_colours.get(m, R.kRed))
                            self.config.addSignalSample(s + "Up%s" % mass_point,  s + "(%s)" % mass_point, self.sig_colours.get(mass_point, R.kRed))
                        else:
                            sys.exit("unknown signal " + s)

        if not bkg: return
        print "Try to add background samples:"
        #self.config.addBackgroundSample("ttbar",  "ttbar", R.kOrange)
        #return
                            
        self.config.addBackgroundSample("VH", "SM H", R.kOrange + 7)
        self.config.addBackgroundSample("ttH", "SM H", R.kOrange + 7)
        
        if len(sig_type) == 1 and sig_type[0] == "ZZ":
            self.config.addBackgroundSample("ZZvvqq", "diboson", R.kGray + 1)                
        else:
            self.config.addBackgroundSample("ZZ", "diboson", R.kGray + 1)            
        self.config.addBackgroundSample("WZ", "diboson", R.kGray + 1)
        self.config.addBackgroundSample("WW", "diboson", R.kGray + 1)

        if tag == "0":
            self.config.addBackgroundSample("stops",  "single top", R.kOrange - 1)
            self.config.addBackgroundSample("stopt",  "single top", R.kOrange - 1)
            self.config.addBackgroundSample("stopWt", "single top", R.kOrange - 1)
            self.config.addBackgroundSample("ttbar",  "ttbar", R.kOrange)
            if not data_fakes and not shade_fakes:
                self.config.addBackgroundSample("ttbarFake",  "ttbar (fake)", R.kOrange - 6)

        if zmg:
            self.config.addBackgroundSample("DYMG", "DYee/#mu#mu", R.kOrange + 1)
            self.config.addBackgroundSample("DYttMG", "DY#tau#tau", R.kOrange + 1)
        else:
            self.config.addBackgroundSample("DY", "DYee/#mu#mu", R.kOrange + 1)
            self.config.addBackgroundSample("DYtt", "DY#tau#tau", R.kOrange + 1)        

        if data_fakes:
            self.config.addBackgroundSample("Fake", "fake", R.kPink+1) 

        if zmg:
            self.config.addBackgroundSample("ZMGl",  "Zee/#mu#mu", R.kMagenta)
            self.config.addBackgroundSample("ZMGcl", "Zee/#mu#mu", R.kMagenta)
            self.config.addBackgroundSample("ZMGcc", "Zee/#mu#mu", R.kMagenta)
            self.config.addBackgroundSample("ZMGbl", "Zee/#mu#mu", R.kMagenta)
            self.config.addBackgroundSample("ZMGbc", "Zee/#mu#mu", R.kMagenta)
            self.config.addBackgroundSample("ZMGbb", "Zee/#mu#mu", R.kMagenta)
        else:
            self.config.addBackgroundSample("Zl",  "Zee/#mu#mu", R.kMagenta)
            self.config.addBackgroundSample("Zcl", "Zee/#mu#mu", R.kMagenta)
            self.config.addBackgroundSample("Zcc", "Zee/#mu#mu", R.kMagenta)
            self.config.addBackgroundSample("Zbl", "Zee/#mu#mu", R.kMagenta)
            self.config.addBackgroundSample("Zbc", "Zee/#mu#mu", R.kMagenta)
            self.config.addBackgroundSample("Zbb", "Zee/#mu#mu", R.kMagenta)


        def addBkg(sample, title, colour, colour_fake = None):
            if colour_fake is None: colour_fake = colour
            self.config.addBackgroundSample(sample, title, colour)
            if split_w_fake and not data_fakes and not shade_fakes:
                #self.config.addBackgroundSample(sample + "Fake", title, colour_fake)
                self.config.addBackgroundSample(sample + "Fake", title + " (fake)", colour_fake)                
            return 

        if wmg:
            if comb_w_flav:
                addBkg("WttMG",  "W#tau#nu",  R.kGreen, R.kGreen + 1)
                addBkg("WMG",  "We#nu/#mu#nu",  R.kGreen - 9, R.kGreen - 6)                
            else:
                addBkg("WttMGl",  "W#tau#nu",  R.kGreen)
                addBkg("WttMGcl", "W#tau#nu", R.kGreen)
                addBkg("WttMGcc", "W#tau#nu", R.kGreen)
                addBkg("WttMGbl", "W#tau#nu", R.kGreen)
                addBkg("WttMGbc", "W#tau#nu", R.kGreen)
                addBkg("WttMGbb", "W#tau#nu", R.kGreen)
                
                addBkg("WMGl",  "We#nu/#mu#nu+l",  R.kGreen - 9)
                addBkg("WMGcl", "We#nu/#mu#nu+cl", R.kGreen - 6)
                addBkg("WMGcc", "We#nu/#mu#nu+cc", R.kGreen + 1)
                addBkg("WMGbl", "We#nu/#mu#nu+bl", R.kGreen + 2)
                addBkg("WMGbc", "We#nu/#mu#nu+bc", R.kGreen + 3)
                addBkg("WMGbb", "We#nu/#mu#nu+bb", R.kGreen + 4)
        else:
            if comb_w_flav:
                #addBkg("Wtt",  "W#tau#nu",  R.kGreen, R.kGreen + 1)
                #addBkg("W",  "We#nu/#mu#nu",  R.kGreen - 9, R.kGreen - 6)                
                addBkg("Wtt",  "Wl#nu",  R.kGreen, R.kGreen - 6)
                addBkg("W",  "Wl#nu",  R.kGreen, R.kGreen - 6)                
            else:
                addBkg("Wttl",  "W#tau#nu",  R.kGreen)
                addBkg("Wttcl", "W#tau#nu", R.kGreen)
                addBkg("Wttcc", "W#tau#nu", R.kGreen)
                addBkg("Wttbl", "W#tau#nu", R.kGreen)
                addBkg("Wttbc", "W#tau#nu", R.kGreen)
                addBkg("Wttbb", "W#tau#nu", R.kGreen)
            
                addBkg("Wl",  "We#nu/#mu#nu+l",  R.kGreen - 9)
                addBkg("Wcl", "We#nu/#mu#nu+cl", R.kGreen - 6)
                addBkg("Wcc", "We#nu/#mu#nu+cc", R.kGreen + 1)
                addBkg("Wbl", "We#nu/#mu#nu+bl", R.kGreen + 2)
                addBkg("Wbc", "We#nu/#mu#nu+bc", R.kGreen + 3)
                addBkg("Wbb", "We#nu/#mu#nu+bb", R.kGreen + 4)

        if zmg:
            self.config.addBackgroundSample("ZttMGl",  "Z#tau#tau+l",  R.kAzure - 9)
            self.config.addBackgroundSample("ZttMGcl", "Z#tau#tau+cl", R.kAzure - 8)
            self.config.addBackgroundSample("ZttMGcc", "Z#tau#tau+cc", R.kAzure - 4)
            self.config.addBackgroundSample("ZttMGbl", "Z#tau#tau+bl", R.kAzure + 1)
            self.config.addBackgroundSample("ZttMGbc", "Z#tau#tau+bc", R.kAzure + 2)
            self.config.addBackgroundSample("ZttMGbb", "Z#tau#tau+bb", R.kAzure + 3)
        else:
            self.config.addBackgroundSample("Zttl",  "Z#tau#tau+l",  R.kAzure - 9)
            self.config.addBackgroundSample("Zttcl", "Z#tau#tau+cl", R.kAzure - 8)
            self.config.addBackgroundSample("Zttcc", "Z#tau#tau+cc", R.kAzure - 4)
            self.config.addBackgroundSample("Zttbl", "Z#tau#tau+bl", R.kAzure + 1)
            self.config.addBackgroundSample("Zttbc", "Z#tau#tau+bc", R.kAzure + 2)
            self.config.addBackgroundSample("Zttbb", "Z#tau#tau+bb", R.kAzure + 3)

            
        if tag != "0":
            self.config.addBackgroundSample("stops",  "single top", R.kOrange - 1)
            self.config.addBackgroundSample("stopt",  "single top", R.kOrange - 1)
            self.config.addBackgroundSample("stopWt", "single top", R.kOrange - 1)
            self.config.addBackgroundSample("ttbar",  "ttbar", R.kOrange)
            if not data_fakes and not shade_fakes:
                self.config.addBackgroundSample("ttbarFake",  "ttbar (fake)", R.kOrange - 6)            

        if shade_fakes:
            self.config.addBackgroundSample("ttbarFake",  "ttbar (fake)", R.kOrange - 6, False) #, 3004)
            self.config.addBackgroundSample("WttFake",  "W#tau#nu (fake)",  R.kGreen - 6, False) #, 3004)
            self.config.addBackgroundSample("WFake",  "We#nu/#mu#nu (fake)", R.kGreen - 3, False) #, 3004)  
            
        #if sig_type[0] == "ZZ":
        #    self.config.addBackgroundSample("hhttbb",  "hh (non-res)", R.kMagenta+4)


        return

    def addSystematics(self):
        "add systs by hand"

        if not self.dosysts: return

        self.config.clearSystematics()
        
        self.config.setSystematicDir("Systematics")

        if self.signal in ["LQ", "LQ3"]:
            self.systs = set(['SysMUON_ID', 'SysTAUS_TRUEHADTAU_EFF_RECO_HIGHPT', 'SysMET_SoftTrk_Scale', 'SysMUON_SCALE', 'SysSubtraction_bkg', 'SysFT_EFF_Eigen_Light_3', 'SysFT_EFF_Eigen_Light_2', 'SysFT_EFF_Eigen_Light_1', 'SysFT_EFF_Eigen_Light_0', 'SysFT_EFF_Eigen_Light_4', 'SysTAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL', 'SysJET_JER_SINGLE_NP', 'SysMUON_ISO_SYS', 'SysTAUS_TRUEHADTAU_SME_TES_INSITU', 'SysFF_MTW', 'SysTAUS_TRUEHADTAU_EFF_RECO_TOTAL', 'SysTAUS_TRUEHADTAU_EFF_JETID_TOTAL', 'SysMUON_ISO_STAT', 'SysEG_RESOLUTION_ALL', 'SysMET_SoftTrk_ResoPerp', 'SysEL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR', 'SysJET_GroupedNP_2', 'SysJET_GroupedNP_3', 'SysJET_GroupedNP_1', 'SysCPVarFakes', 'SysFT_EFF_Eigen_B_0', 'SysFT_EFF_Eigen_B_2', 'SysEL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR', 'SysMUON_EFF_TrigSystUncertainty', 'SysFT_EFF_extrapolation', 'SysFT_EFF_Eigen_C_0', 'SysFT_EFF_Eigen_C_1', 'SysFT_EFF_Eigen_C_2', 'SysFT_EFF_Eigen_C_3', 'SysMUON_MS', 'SysMUON_TTVA_STAT', 'SysMUON_EFF_SYS', 'SysFT_EFF_Eigen_B_1', 'SysMET_SoftTrk_ResoPara', 'SysEL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR', 'SysFFStatQCD', 'SysMUON_EFF_TrigStatUncertainty', 'SysTAUS_TRUEHADTAU_SME_TES_MODEL', 'SysMUON_EFF_STAT', 'SysANTITAU_BDT_CUT', 'SysFT_EFF_extrapolation_from_charm', 'SysTAUS_TRUEHADTAU_SME_TES_DETECTOR', 'SysEL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR', 'SysTAUS_TRUEHADTAU_EFF_JETID_HIGHPT', 'SysFFStatTtbar', 'SysMUON_TTVA_SYS', 'SysEG_SCALE_ALL', "SysSS", "SysTTBAR_NNLO", "SysMUON_SAGITTA_RESBIAS", "SysMUON_SAGITTA_RHO", "SysMUON_EFF_STAT_LOWPT", "SysMUON_EFF_SYS_LOWPT", "SysJET_JvtEfficiency", "SysPRW_DATASF", "SysTTbarGenFakes", 'SysJET_21NP_JET_Pileup_OffsetNPV', 'SysJET_21NP_JET_Flavor_Response', 'SysJET_21NP_JET_EffectiveNP_1', 'SysJET_21NP_JET_EtaIntercalibration_NonClosure', 'SysJET_21NP_JET_Pileup_PtTerm', 'SysJET_21NP_JET_EffectiveNP_2', 'SysJET_21NP_JET_EffectiveNP_3', 'SysJET_21NP_JET_EffectiveNP_6', 'SysJET_21NP_JET_EffectiveNP_7', 'SysJET_21NP_JET_EffectiveNP_4', 'SysJET_21NP_JET_EffectiveNP_5', 'SysJET_21NP_JET_EffectiveNP_8restTerm', 'SysJET_21NP_JET_SingleParticle_HighPt', 'SysJET_21NP_JET_BJES_Response', 'SysJET_21NP_JET_Pileup_RhoTopology', 'SysJET_21NP_JET_Pileup_OffsetMu', 'SysJET_21NP_JET_Flavor_Composition', 'SysJET_21NP_JET_EtaIntercalibration_TotalStat', 'SysJET_21NP_JET_PunchThrough_MC15', 'SysJET_21NP_JET_EtaIntercalibration_Modelling', 'SysCompFakes', 'SysTTbarST', 'SysZtautauST', 'SysZtautauPTTau'])
        else:
            self.systs = set(['SysMUON_ID', 'SysTAUS_TRUEHADTAU_EFF_RECO_HIGHPT', 'SysMET_SoftTrk_Scale', 'SysMUON_SCALE', 'SysSubtraction_bkg', 'SysFT_EFF_Eigen_Light_3', 'SysFT_EFF_Eigen_Light_2', 'SysFT_EFF_Eigen_Light_1', 'SysFT_EFF_Eigen_Light_0', 'SysFT_EFF_Eigen_Light_4', 'SysTAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL', 'SysJET_JER_SINGLE_NP', 'SysMUON_ISO_SYS', 'SysTAUS_TRUEHADTAU_SME_TES_INSITU', 'SysFF_MTW', 'SysTAUS_TRUEHADTAU_EFF_RECO_TOTAL', 'SysTAUS_TRUEHADTAU_EFF_JETID_TOTAL', 'SysMUON_ISO_STAT', 'SysEG_RESOLUTION_ALL', 'SysMET_SoftTrk_ResoPerp', 'SysEL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR', 'SysJET_SR1_JET_GroupedNP_2', 'SysJET_SR1_JET_GroupedNP_3', 'SysJET_SR1_JET_GroupedNP_1', 'SysCPVarFakes', 'SysFT_EFF_Eigen_B_0', 'SysFT_EFF_Eigen_B_2', 'SysEL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR', 'SysMUON_EFF_TrigSystUncertainty', 'SysFT_EFF_extrapolation', 'SysFT_EFF_Eigen_C_0', 'SysFT_EFF_Eigen_C_1', 'SysFT_EFF_Eigen_C_2', 'SysFT_EFF_Eigen_C_3', 'SysMUON_MS', 'SysMUON_TTVA_STAT', 'SysMUON_EFF_SYS', 'SysFT_EFF_Eigen_B_1', 'SysMET_SoftTrk_ResoPara', 'SysEL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR', 'SysFFStatQCD', 'SysMUON_EFF_TrigStatUncertainty', 'SysTAUS_TRUEHADTAU_SME_TES_MODEL', 'SysMUON_EFF_STAT', 'SysANTITAU_BDT_CUT', 'SysFT_EFF_extrapolation_from_charm', 'SysTAUS_TRUEHADTAU_SME_TES_DETECTOR', 'SysEL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR', 'SysTAUS_TRUEHADTAU_EFF_JETID_HIGHPT', 'SysFFStatTtbar', 'SysMUON_TTVA_SYS', 'SysEG_SCALE_ALL', "SysSS", "SysTTBAR_NNLO", "SysTTbarMBB", "SysTTbarPTH", "SysZtautauMBB", "SysZtautauPTH", "SysMUON_SAGITTA_RESBIAS", "SysMUON_SAGITTA_RHO", "SysMUON_EFF_STAT_LOWPT", "SysMUON_EFF_SYS_LOWPT", "SysJET_JvtEfficiency", "SysPRW_DATASF", "SysTTbarGenFakes", 'SysJET_21NP_JET_Pileup_OffsetNPV', 'SysJET_21NP_JET_Flavor_Response', 'SysJET_21NP_JET_EffectiveNP_1', 'SysJET_21NP_JET_EtaIntercalibration_NonClosure', 'SysJET_21NP_JET_Pileup_PtTerm', 'SysJET_21NP_JET_EffectiveNP_2', 'SysJET_21NP_JET_EffectiveNP_3', 'SysJET_21NP_JET_EffectiveNP_6', 'SysJET_21NP_JET_EffectiveNP_7', 'SysJET_21NP_JET_EffectiveNP_4', 'SysJET_21NP_JET_EffectiveNP_5', 'SysJET_21NP_JET_EffectiveNP_8restTerm', 'SysJET_21NP_JET_SingleParticle_HighPt', 'SysJET_21NP_JET_BJES_Response', 'SysJET_21NP_JET_Pileup_RhoTopology', 'SysJET_21NP_JET_Pileup_OffsetMu', 'SysJET_21NP_JET_Flavor_Composition', 'SysJET_21NP_JET_EtaIntercalibration_TotalStat', 'SysJET_21NP_JET_PunchThrough_MC15', 'SysJET_21NP_JET_EtaIntercalibration_Modelling', 'SysCompFakes'])            

        for s in self.systs:
            if "21NP" in s: continue # No 21NP for GC
            if "ANTITAU_BDT_CUT" in s: continue # Not used anymore as replaced
            ####if "Eigen_C_3" in s: continue
            #if s in ["SysSubtraction_bkg"]: continue
            #if s in ["SysFFStatTtbar", "SysFFStatQCD", "SysANTITAU_BDT_CUT"]: continue
            self.config.addSystematic(s, True if ("JER" in s or "ANTITAU" in s or "NNLO" in s or "SS" in s or "FF_MTW" in s or "MT40GeV" in s or 
                                                  "FF_MTW" in s or "MT40GeV" in s or "ResoPerp" in s or "ResoPara" in s) else False) 
        return

    def makePlotsImpl(self, dirs, names = [], tags = None):
        print ">>> ---------------------------------"
        print ">>>  plotting2Tau.py::makePlotsImpl"
        print ">>> ---------------------------------"
        # Loop over tags
        if tags is None: 
            print 'The tags argumet is Null, thus the defulat value will be configured.'
            print tags
            tags = self.tags

        for t in tags:        
            tag = str(t)

            # Load file for correct tt
            # TODO: deal more carefully with 1p, 2p, 3p ... so uses lowest tag (currently 0)
            try:
                p = self.paths[t] if self.tt else self.paths[0]            
            except TypeError:
                p =  self.paths[0]
                
            print ">>> Processing tag", tag, "with path", p + "\n"
            self.config.setInputPath(p)

            # Loop over variables sets for those requested
            for d, varSet in self._varMap.iteritems():
                if not d in dirs: continue
                self.cd(d)                
                vars = [n.split('#')[1] for n in names if '#' in n and n.split('#')[0] == d]

                # Loop over "regions"
                for r in varSet.regions:
                    print '>>> Region : ' + r

                    # Add region + label
                    title = tag + " tags"
                    title += ((", " + varSet.label) if varSet.label is not None else "")
                    rn = varSet.region_names.get(r, r)
                    title += ((", " + rn) if rn else "")
                    slideTitle = d + ", " + title
                    region = tag + "tag2pjet_0ptv" + ("_{}".format(r) if r else "")
                    self.addRegion(region , title, slideTitle)

                    # Add samples
                    sig = False if ('SS' in r or t == 0 or not varSet.signal) else True
                    
                    print '>>> addSamples() is called.'
                    print '>>> tag        : ' + str(tag)
                    print '>>> sig        : ' + str(sig)
                    print '>>> mass       : ' + str(varSet.mass)
                    print '>>> sig_type   : ' + str(varSet.signal)
                    print '>>> data_fakes : ' + str(self.data_fakes)
                    self.addSamples(
                            tag,
                            sig, 
                            varSet.mass,
                            sig_type = varSet.sig_type,
                            data_fakes = self.data_fakes,
                            )           

                    # Loop over individual variables
                    print '>>> Loop over indivisual variables (rebin & addVariable): '
                    print '>>> variables :'
                    for v in varSet.vars:
                        print '>>>   ' + v.var
                        if v.level > self.level: continue
                        if vars and not v.var in vars: continue

                        # Set binning
                        # TODO: deal more carefully with 1p, 2p, 3p ... so uses lowest tag (currently 0)
                        try:
                            rebin = v.bins[t] if isinstance(v.bins, list) else v.bins
                        except TypeError:
                            rebin = v.bins[0]

                        # should be moved somwhere less central and be more steerable
                        if 'SS' in r and not isinstance(rebin, tuple):  rebin *= v.ssfact                        

                        self.addVariable(v.var, v.name, v.lo, v.hi, rebin, divide_width = v.divide_width, var_type = v.var_type if sig else None) 

                        if v.sysplots and region in v.sysplots:
                            self.config.addDetailedSystematicsPlots(v.var, "", region, "Systematics:")

                    massStr = (varSet.sig_type if varSet.sig_type is not None else self.signal) + varSet.mass.replace(',','_') if isinstance(varSet.mass, basestring) else varSet.mass
                    outdir = self.outname + "/{}/{}/{}/".format(massStr, varSet.dirname, self.sanitise(t)) + ("{}/".format(self.sanitise(r)) if r else "")                        
                    print ">>> outdir = "  + str(outdir)
                    self.config.setOutputPath(outdir)
                    if varSet.pdf: self.makePdf(outdir)
                    self.plotter = R.PlotMaker()                     
                    
                    print ">>> PlotMaker has been instantiated, and plotting2Tau::makePlots() is called."
                    self.plotter.makePlots(self.config)
                    
                    print ">>> makePlots() has been finished, and clearVariables will be started."
                    self.config.clearVariables()
                    
                    self.config.clearSamples()                    
                    self.config.clearRegions()
        return

