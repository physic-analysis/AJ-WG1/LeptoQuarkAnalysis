import ROOT as R

import os, sys, re
from array import array
import multiprocessing as mp
from shutil import copyfile, copy
from collections import namedtuple
from collections import OrderedDict as odict

from mp import parallel_exec
from brace_expansion import expand

def uniqify(seq):
    "fast remove duplicates in a sequence `seq` while preserving order"
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]

# Struct describing a variable
var = namedtuple('var', ['var', 'name', 'lo', 'hi', 'bins', 'level', 'ssfact', 'divide_width', 'sysplots', 'var_type', 'region_select', 'region_skip', 'ylog_min', 'ylog_max'])
var.__new__.__defaults__ = (-1, -1, 1, 0, 2, False, False, None, [], [], -1.0, -1.0)  # applies to len right most 

# Struct describing a set of variables
varset = namedtuple('varset', ['dir', 'vars', 'dirname', 'regions', "region_names", 'mass', 'signal', 'label', 'pdf', 'sig_type'])
varset.__new__.__defaults__ = (None, [], {}, "300", True, None, None) # applies to len right most 

# Augmented dictionary to keep lists of plotters keyed by outdir, also keeps track of last sample
class PlotterDict(dict):
    def __init__(self):
        dict.__init__(self)
        self.last = None
    def __iadd__(self, rhs):
        self[rhs.outname] = self.last = rhs
        return self

# Main plotting class to do all the hard work
class Plotter(object) :

    loaded = False
    
    def __init__(self, dname, inname, outname, title, lumi = "3.2", tt = True, signal = "", mass = "300", regions=[], jets = [], tags = [], tagjets = [],
                 descr = [], data_fakes = False, level=0, systs = True, read_systs=True, backup=False, outpath = '.', topfakerate = False, ttbarfakerateonly=False):
        

        print 'plotting.py: init with inname',inname

        print "test 1"
        R.gROOT.SetBatch(True)
        print "test 2"
        R.gStyle.SetOptStat(False)
        print "test 3"

        # Load C++
        self.load()
        print "test 4"

        # Config setup
        self.config = R.Config()
        print "test 5"
        self.paths = ([dname] * 3) if isinstance(dname, basestring) else dname
        self.config.setInputFile(inname)
        self.config.addBDTTransformation("")
        self.config.setNoSignalStack()
        self.config.clearRegions()
        self.config.clearSamples()
        self.config.clearVariables()
        self.config.setAnalysis( R.Config.TwoLepton,  
                                 "Internal",         
                                 "",         
                                 "13",               
                                 lumi,           
                                 "300",              
                                 "Liverpool",             
                                 "v1.0")
        self.config.setAnalysisTitle(title)
        self.config.scaleSignalToBackground(True)
        self.config.multiSignal(True)
        
        # Local setup
        self.mass = mass
        self.sig_colours = {}
        self.outname = outname
        self.jets = jets
        self.tags = tags
        self.tagjets = tagjets
        self.descr = descr        
        self.regions = regions
        self.tt = tt
        self.level = level
        self.signal = signal
        self.data_fakes = data_fakes
        self.topfakerate = topfakerate
        self.ttbarfakerateonly = ttbarfakerateonly
        self.dosysts = systs
        print 'plotting.py(init) setting dosysts',self.dosysts
        self.read_systs = read_systs
        print 'plotting.py(init) setting read systs',self.read_systs

        # deal with parsing the regions here
        #self.pat = re.compile("(?P<tag>\\dp?)tag((?P<fatjet>\\dp)?fat)?(?P<jet>\\dp?)?jet_(?P<ptv>\\d+(_\\d+)?)ptv_?(?P<descr>[a-zA-Z0-9{},]+)?")
        self.pat = re.compile("(?P<tag>\\dp?)tag((?P<fatjet>\\dp)?fat)?(?P<jet>\\dp?)?jet_(?P<ptv>[_a-zA-Z0-9{},]+(_[_a-zA-Z0-9{},]+)?)ptv_?(?P<descr>[a-zA-Z0-9{},]+)")

        self._varMap = {}

        if backup:
            self.backUp(inname)

        pass

    def load(self):
        "Load/compile the necessary plotting tools"

        print 'plotting.py: load() load C++?',Plotter.loaded
        if Plotter.loaded: return
        print "1"

        #  libdir = "PlottingTool/"
        libdir = "./PlottingTool/core/"
        
        print "2"
        includePath = R.gSystem.GetIncludePath()
        includePath = '-I$ROOTSYS/include -I"/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/LeptoQuark/TI_LqAnalysis/MIA_CC/build/x86_64-slc6-gcc62-opt/include" -I"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.60/InstallArea/x86_64-slc6-gcc62-opt/include" -I"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.60/InstallArea/x86_64-slc6-gcc62-opt/include" -I"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.60/InstallArea/x86_64-slc6-gcc62-opt/include/libxml2" -I"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.60/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3" -I"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.60/InstallArea/x86_64-slc6-gcc62-opt/etc" -I"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.60/InstallArea/x86_64-slc6-gcc62-opt/etc/cling" -I"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.60/InstallArea/x86_64-slc6-gcc62-opt/include" -I"/build1/atnight/localbuilds/nightlies/21.2/AnalysisBase/build/build/AnalysisBaseExternals/x86_64-slc6-gcc62-opt/include/python2.7"'
        print includePath
        print "3"
        R.gSystem.SetIncludePath(includePath + " -ITransformTool")
        print "4"
        R.gROOT.ProcessLine(".L ./TransformTool/Root/HistoTransform.cxx+")
        print "5"
        R.gROOT.ProcessLine(".L " + libdir + "Utils.cxx+")
        print "6"
        R.gROOT.ProcessLine(".L " + libdir + "SystTool.cxx+")
        print "7"
        R.gROOT.ProcessLine(".L " + libdir + "Config.cxx+")
        print "8"
        R.gROOT.ProcessLine(".L " + libdir + "LatexMaker.cxx+")
        print "9"
        R.gROOT.ProcessLine(".L " + libdir + "PlotMaker.cxx+")
        print "10"

        print "11"
        Plotter.loaded = True
        return

    def backUp(self, name):
        "WIP, untested"

        for i,p in enumerate(self.paths[0]):  # Hack for now
            backpath = os.path.expandvars("/hepstore/$USER/MIA/Output/{}".format(self.outname))
            if os.path.exists(backpath + "/{}.root".format(name)):
                self.paths = [backpath] * 3
            else:
                print "Backing up file to", backpath
                os.makedirs(backpath)
                copy("{}/{}.root".format(p, name), "{}".format(backpath))                
            
        return 

    def decodeRegion(self, r):
        "Decode a region name based on a regexp from `self.pat` and form a dict or name and value"
        if not r: return {}
        res = self.pat.match(r)
        if not res: return {}
        d = res.groupdict()
        d["tagjet"] = r.split('_')[0]
        return d

    def addSamples(self, tag, sig, mass, sig_type = None):
        "Add list of signal, bkg, data samples - overrise in derived class"
        raise NotImplementedError

    def addSF(self, sample, scaleFactor, regionExp = "*"):

        if sample == "*":
            self.addSamples()
            for s in self.config.getSamples():
                if s.type == R.Config.Data: continue
                print "A. Applying SF {} to sample {} for region {}".format(scaleFactor, s.name, regionExp)                 
                self.config.addScaleFactor(s.name, regionExp, scaleFactor);
                self.config.clearSamples()                    
        else:                                
            print "B. Applying SF {} to sample {} for region {}".format(scaleFactor, sample, regionExp)             
            self.config.addScaleFactor(sample, regionExp, scaleFactor)

        return 


    def switchBackgroundSample(self, oldname, newname, title = "", color = R.kOrange):
        "Switch a bkg to a different one; useful for systematic studies"
        if not title: title = newname
        self.config.removeSample(oldname)
        self.config.addBackgroundSample(newname, title, color)
        return
    
    def cd(self, name):
        "Chose input directory"
        self.config.setInputTDirectory(name);
        return

    def sanitise(self, region):
        "Sanitise a region name for using in output file/dir names"
        try:
            if '{' in region:
                return  region.replace(" ", "").replace("{", "").replace("}", "").replace(",", "+")
            else:
                return region
        except TypeError:
            return region

        
    def addRegion(self, region, title = None, slide = None, region_name = None):
        "Add a region, allowing to combine regions based on bash-like brace {} expansion"

        if '{' in region:
            if region_name is None:
                region_name = region
            combreg = self.sanitise(region_name)

            for r in expand(region):
                self.config.addRegion(r, combreg, title if title is not None else region, slide if slide is not None else region)
        else:
            self.config.addRegion(region, title if title is not None else region, slide if slide is not None else region)

        return

    def addVariable(self, var, name = None, lo = -1, hi = -1, rebin = -1, divide_width = False, var_type = None, ylog_min = 1.0, ylog_max = 1.0):
        """
        Add a variable

        If rebin is a tuple () use variable rebinning, else normal rebinning
        """

        if var_type is None:
            var_type = R.Config.BDTInput

        if isinstance(rebin, tuple):
            bins = R.vector(R.Double)()
            bins += rebin            
            self.config.addVariable(var_type, var, name if name is not None else var, bins, divide_width, 1, True, False) #, ylog_min, ylog_max)
        else:
            self.config.addVariable(var_type, var, name if name is not None else var, lo, hi, rebin, 1, True, False) #, ylog_min, ylog_max)
        return

    def addVarSet(self, name, *varlist, **kw):
        "Add a set of variables, which correpons to a directory in the input file"
        self._varMap[name] = varset(name, varlist,
                                    dirname = kw.get("dirname", name),                                   
                                    regions = kw.get("regions", self.regions),
                                    region_names = kw.get("region_names", {}),                                    
                                    mass = kw.get("mass", self.mass),                                   
                                    signal = kw.get("signal", True),
                                    label = kw.get("label", None),
                                    pdf = kw.get("pdf", False),
                                    sig_type = kw.get("sig_type", self.signal),
                                    )                                   
        return 

    def clearSet(self):
        "Clear the set of variables"
        self._varMap.clear()
        return

    def readSetup(self):
        "Read the setup from the file for regions, systs etc"

        self.config.setInputPath(self.paths[0])

        print 'readSetup, readsysts',self.read_systs
        print 'readSetup, dosysts',self.dosysts
        if self.read_systs:
            self.config.setReadSysts(True)
            self.config.setSystematicDir("Systematics")
        else:
            # Force not to read systs automatically
            #self.config.setReadSysts(False)
            # Need to commit to PlottingTool
            pass

        # Read regions (+ systs)
        self.config.readSetup()
        regions = sorted([r.name for r in self.config.getRegions()])
        self.config.clearRegions()
        
        # Set systs by hand
        if not self.read_systs:
            print addSystematics
            self.addSystematics()        

        if not self.dosysts:
            print 'clearSystematics'
            self.config.clearSystematics()

        self.systs = [s for s in self.config.getSystematics()]
        print 'have nsysts: ', self.config.getNSystematics()

        return regions

    def addSystematics(self):
        "add systs by hand - to be implemented in derived class"            
        raise NotImplementedError

    def makePdf(self, din):
        "Make pdf of a variable set (i.e. dir)"
        pid = str(os.getpid())
        os.system("convert " + din + "*.png /tmp/out." + pid + ".pdf && pdfjam --nup 2x2 -o " + din + "plots.pdf /tmp/out." + pid + ".pdf && rm /tmp/out." + pid + ".pdf")
        # gs -o test.eps -sDEVICE=ps2write -dBATCH -dEPSCrop -dEPSFitPage  <*.eps>
        return 

    def makePlots(self, names = [], tags = None, nproc=1):
        "Top-level plotting function dealing with running on `nproc` cores in parallel"

        print ">>> plotting::makePlots --- "
        # Variable sets to run on 
        if not names: names = self._varMap.keys()
        names = [names] if isinstance(names, basestring) else names

        # Vars withing variable sets to run on
        dirs = [n.split('#')[0] for n in names]

        print ">>> Running over variable sets:", uniqify(dirs)
        if int(nproc) == 1:
            print '>>>> The number of procceses (nproc) : ' , nproc
            print '>>>>  makePlotsImpl argument : '
            print dirs
            print names
            print tags
            self.makePlotsImpl(dirs, names, tags)
        else:           
            print 'how can be here',nproc 
            parallel_exec(target=self.makePlotsImpl,
                          args_list=[(d, names, tags) for d in dirs],
                          nproc = nproc, sleep = 60,
                          name = lambda args : args[0])                          
        return


    def makePlotsImpl(self, dirs, names = [], tags = None):
        "Main plotting function that does the hard work. you can overwrite it in your derived plotting class"
        

        # Set path
        p =  self.paths[0]
        print ">>> Using path",p
        self.config.setInputPath(p)

        # Loop over variables sets for those requested
        for d, varSet in self._varMap.iteritems():
            # Only plot those requested
            if not d in dirs: continue
            # Cd to correct dir
            self.cd(d)
            # Split var name from dir name
            vars = [n.split('/')[1] for n in names if '/' in n and n.split('/')[0] == d]            

            # If no regions defined for varSet, load all regions from file (needs to be done for each varSet (i.e. folder) as may change
            regions = varSet.regions
            if not regions:
                regions = self.readSetup()                
#             else:
#                 self.readSetup()

            # Loop over regions, keeping track of which have been seen
            regions_seen = set()            
            for r in regions:
                print ">>> Processing region", r

                region_name = varSet.region_names.get(r, r)

                # Decode region based on regex to get info
                regDict = self.decodeRegion(region_name)
                print 'region_name',region_name
                print 'regDict',regDict
                tag = regDict.get("tag", "")
                jet = regDict.get("jet", "")
                descr = self.sanitise(regDict.get("descr", ""))
                tagjet = regDict.get("tagjet", "")                                

                # Allow to write in same dir for diff PtV bins 
                if (tagjet, descr) in regions_seen:
                     self.config.setOverwrite(False)
                else:
                    self.config.setOverwrite(True)
                    regions_seen.add((tagjet, descr))

                # Filter on tags, jets, etc (i.e. parts of region rather than full region)
                #print 'region_name,regDict',region_name,regDict
                #print 'self.tags',self.tags,'tag',tag    
                if self.tags and not tag in self.tags: continue
                if self.jets and not jet in self.jets: continue
                if self.tagjets and not tagjet in self.tagjets: continue
                if self.descr and not descr in self.descr: continue                                

                # Add region, setting title
                title = (("#geq " + tag[:-1]) if tag.endswith("p") else tag) + " tags, "                
                title += (("#geq " + jet[:-1]) if jet.endswith("p") else jet) + " jets"
                title += ((", " + varSet.label) if varSet.label is not None else "")
                title += ((", " + descr) if descr else "")
                # if ptv in region make a title from it
                title_ptv=""
                ptv = regDict.get("ptv", "")
                ptvsplit=ptv.split('_')
                if len(ptvsplit) > 1:
                    lo=ptvsplit[0]
                    hi=ptvsplit[1]
                    title_ptv += (lo+" < p_{T}^{V} #leq "+hi+" GeV")
                elif len(ptvsplit) == 1:                    
                    title_ptv += ("p_{T}^{V} > "+ptvsplit[0]+" GeV")
                #
                # print 'title_ptv',title_ptv 
                # split the lines if there is a ptv title and add it to the (region) title
                if len(title_ptv)>0:
                    title = "#splitline{"+title+"}{"+title_ptv+"}"
                print 'title',title    
                region = "{}".format(r) if r else ""
                # remember slideTitle only used for latex
                slideTitle = d + ", " + title
                self.addRegion(region , title, slideTitle, region_name = region_name)
                
                # Add samples
                sig = False if (tag == "0" or not varSet.signal) else True
                sig = True;
                self.addSamples(tag, sig, varSet.mass, varSet.sig_type)
                
                # Loop over individual variables within varset
                for v in varSet.vars:
                    # Continue if not in vars reuqested or has lelve below that plotting (latter not really used yet)
                    if v.level > self.level: continue
                    if vars and not v.var in vars: continue
                    #if v.region_select and region not in v.region_select: continue
                    #if v.region_skip and region in v.region_skip: continue
                    
                    # Set binning:
                    # - if a list, each entry corresponds to diff binnings for 0,1,2 tag. Within this
                    # - if a number use as simple rebinning
                    # - if a tuple use as bin edges
                    # - if a dist, allow different binnings keeyed by region
                    try:
                        rebin = v.bins[tag] if isinstance(v.bins, list) else v.bins[region] if isinstance(v.bins, dict) else v.bins
                        print "rebin @@@@@@@@@@@@@@@@@@@@@@@@@@@@" + str(rebin)
                    except TypeError:
                        rebin = v.bins[0]
                    except KeyError:
                        rebin = v.bins.get("", 1)

                    # Add variables and, optionally, detailed syst plots
                    self.addVariable(v.var, v.name, v.lo, v.hi, rebin, divide_width = v.divide_width, var_type = v.var_type if varSet.signal else None, ylog_min = v.ylog_min, ylog_max = v.ylog_max)
                        
                    if v.sysplots and region in v.sysplots:
                        self.config.addDetailedSystematicsPlots(v.var, "", region, "Systematics:")

                # Construct output directory naming
                massStr = (varSet.sig_type if varSet.sig_type is not None else self.signal) + varSet.mass.replace(',','_') if isinstance(varSet.mass, basestring) else varSet.mass
                outdir = self.outname + "/{}/{}/{}/".format(massStr, varSet.dirname, tagjet) + ("{}/".format(descr) if descr else "")                                        

                # Make plots
                self.config.setOutputPath(outdir)
                if varSet.pdf: self.makePdf(outdir)
                self.plotter = R.PlotMaker()
                self.plotter.makePlots(self.config)                

                # Clean up
                self.config.clearVariables()
                self.config.clearSamples()                    
                self.config.clearRegions()

            #self.config.setOverwrite(True)
        return
    
    def readHist(self, directory, regions, var, group=False, samples = [], bkg_veto = [], rebin = 1, stack=False, xtitle = "", divide_width=False,
                 scale = {}, scale_bkg_ff = False, trafo = None):
        """
        Read a histogram `var` in `directory` from the file for `regions` for use in non-standard plotting/studies

        If not `stack`, returns a map of samples keyed by name (or title if `group=True`) 
        and includes total background sum (minus `bkg_veto` list) as "bkg"

        if `stack` returns (data, bkg stack, [signals])

        """
        
        regions = [regions] if isinstance(regions, basestring) else regions
        if directory:
            directory += "/"

        stack_names = []
        sig_names = []
        hists = odict()
        hbkg = None

        if not xtitle: xtitle = var

        # Loop over regions to be added
        for r in regions:
            try:
                t = r[0]
                p = self.paths[t] if self.tt else self.paths[0]            
            except TypeError:
                p =  self.paths[0]

            print ">>> Reading hist", var, "with path", p, "for region", r
            self.config.setInputPath(p)
            f = R.TFile.Open(self.config.getInputFileFullName(""))

            # Loop over samples
            for s in self.config.getSamples():
                if samples and not (s.name in samples or s.title in samples):
                    continue

                # Retrieve hist
                hname = directory + s.name + "_" + r + "_" + var
                key = s.title if group else s.name
                #print hname
                h = f.Get(hname)

                # Fall back to nominal
                if not h and "Sys" in hname:
                    print "WARNING: Hist %s not found -> faling back to nominal" % hname
                    hname = hname.replace("/Systematics", "").split("_Sys")[0]
                    h = f.Get(hname)

                if not h: continue

                # rebin
                if trafo is None:
                    try:
                        h = h.Rebin(len(rebin)-1, h.GetName() + "_rebin", array("d", rebin))
                    except TypeError:
                        if rebin > 1:
                            h = h.Rebin(rebin, h.GetName() + "_rebin")                

                # Scale
                if scale and h:
                    if s.name in scale:
                        h.Scale(scale[s.name])
                    if s.title in scale:
                        h.Scale(scale[s.title])                    

                if divide_width and h:
                    h.Scale(1, "width")

                h.SetDirectory(0)

                # Combine regions and samples for each group of samples
                try:                    
                    hists[key].Add(h)                    
                except KeyError:
                    # Style
                    h.SetLineColor(s.lineColor)
                    h.SetFillColor(s.fillColor)
                    try:
                        h.SetFillStyle(s.fillStyle)                    
                    except AttributeError:
                        pass
                    h.SetMarkerColor(s.markerColor)

                    h.GetXaxis().SetTitle(xtitle)
                    h.GetYaxis().SetTitle("Events")                    

                    if key == "data" or key == "Data":
                        h.SetMarkerStyle(20)                    
                        h.SetMarkerSize(0.5)                    

                    hists[key] = h


                if stack:
                    if (s.type != R.Config.Data and s.type != R.Config.Signal):
                        stack_names.append(key)
                if s.type == R.Config.Signal:
                    sig_names.append(key)
                        
                
                # Combine total bkg
                if (s.type != R.Config.Data and s.type != R.Config.Signal
                    and not (s.name in bkg_veto or s.title in bkg_veto) ):

                    if scale and "bkg" in scale and h:
                        h.Scale(scale["bkg"])

                    if scale_bkg_ff and h:
                        # 6% normal XS for ttbar and 50% for all others
                        if "ttbar" in s.name:
                            h.Scale(1 + scale_bkg_ff * 0.06)
                        else:
                            h.Scale(1 + scale_bkg_ff * 0.50)                            

                    try:
                        hbkg.Add(h)
                    except AttributeError:
                        hbkg = h.Clone()
                        hbkg.SetTitle("Bkg")
                        hbkg.SetLineColor(1)
                        hbkg.SetFillColor(29)                        
                        hbkg.SetDirectory(0)

        if stack:
            hs = R.THStack()
            [hs.Add(hists[k]) for k in uniqify(stack_names)]

            try:
                data = hists['data']
            except KeyError:
                data = hists['Data']                
                
            return data, hs, [hists[k] for k in uniqify(sig_names)] 


        hists["bkg"] = hbkg

        try:
            hists['Data'] = hists['data']
        except KeyError:            
            hists['data'] = hists['Data']
            
        f.Close()

        if trafo is not None:
            bins = R.Utils.getRebinBins(hists['bkg'], [hists[k] for k in uniqify(sig_names)][0], trafo[1], trafo[2], trafo[3])
            for h in hists.itervalues():
                R.Utils.rebinHisto(h, bins)

        return hists

    def makeLegend(self, pos = None):
        "Make a legend at `pos` for use with `readHists`"

        if pos is None:
            pos = (0.78,0.18,0.89,0.89)

        leg = R.TLegend(*pos)
        leg.SetFillColor(10)
        leg.SetBorderSize(0)
        leg._hists = []

        samples = self.config.getSamples()
        data = [s for s in samples if  s.type == R.Config.Data]
        signal = [s for s in samples if  s.type == R.Config.Signal]
        bkg = [s for s in samples if  s.type != R.Config.Signal and s.type != R.Config.Data]        

        samplelist = bkg + signal + data

        done = []
        for s in reversed(samplelist):
            if s.title in done: continue
            done.append(s.title)

            if s.type == R.Config.Data:
                opt = "lp"
            else:
                opt = "l" if self.config.getNoStack() else 'f'

            dirstat = R.TH1.AddDirectoryStatus()
            R.TH1.AddDirectory(False)
            htmp = R.TH1F(s.name, s.title, 1, 0, 1)
            htmp.SetLineColor(s.lineColor)
            htmp.SetFillColor(s.fillColor)
            htmp.SetMarkerStyle(20)                    
            htmp.SetMarkerSize(0.5)   
            R.TH1.AddDirectory(dirstat)

            leg.AddEntry(htmp, s.title, opt)
            leg._hists.append(htmp)

        return leg

    def findSysts(self):
        "Extract systs from file and print but SLOW so use with caution.  Useful for filling `addSystematics`" 

        def addSystsForDir(d, systs):
            names = [k.GetName() for k in d.GetListOfKeys() if not k.IsFolder() and "_Sys" in k.GetName()]
            short_names = [n[n.find("Sys"):n.find("__") if n.find("__")>0 else None] for n in names]            
            return systs.update(short_names)

        systs = set()

        self.config.setInputPath(self.paths[0])
        f = R.TFile.Open(self.config.getInputFileFullName(""))
        for k in f.GetListOfKeys(): 
            if not k.IsFolder(): continue

            if k.GetName() == "Systematics":
                d = k.ReadObj()
                addSystsForDir(d, systs)
                continue

            d = f.GetDirectory(k.GetName() + "/Systematics")
            if not d: continue
            addSystsForDir(d, systs)

        print systs

        return

